<?php

namespace ADW\IlpCrmBundle\Entity;

use JMS\Serializer\Annotation as Serialized;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;

/**
 * Class Participant
 * @package ADW\IlpCrmBundle\Model\Response
 * @ORM\Entity()
 */
class Participant implements UserInterface, EquatableInterface
{
    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     *
     */
    private $password;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    private $salt;

    /**
     * @var array
     * @Serialized\Type("array")
     * @Serialized\XmlElement(cdata=false)
     */
    private $roles = ['ROLE_USER'];

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="NONE")
     */
    protected $id;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $login;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $birthdate;

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $age;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $uid;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $isphoneactivated;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $mobileactivationcode;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $isactivated;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $ismailingagreed;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $isrulesagreed;

    /**
     * @var string
     * @Serialized\Type("string")
     */
    protected $ismale;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $ispdagreed;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $mobilephone;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $vk_id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $ok_id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $fb_id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $gp_id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $tw_id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     */
    protected $mailru_id;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $registrationdt;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $phoneactivationdt;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $activationdt;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $email;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $firstname;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $lastname;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $isemailactivated;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $mailactivationcode;


    public function __construct($username = null, $password = null, $salt = null, $roles = array(), $firstname = null, $bdate = null, $sex = null, $socials = array())
    {
        $this->email = $username;
        $this->password = $password;
        $this->salt = $salt;
        $this->roles = $roles;
        $this->firstname = $firstname;
        $this->birthdate = $bdate;
        $this->ismale = $sex;
        if (count($socials)) {
            foreach ($socials as $soc => $id) {
                $property = $soc . '_id';
                $this->$property = $soc . ' ' . $id;
            }
        }
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @param string $login
     */
    public function setLogin($login)
    {
        $this->login = $login;
    }

    /**
     * @return string
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param string $uid
     * @return Participant
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsphoneactivated()
    {
        return $this->isphoneactivated;
    }

    /**
     * @param string $isphoneactivated
     * @return Participant
     */
    public function setIsphoneactivated($isphoneactivated)
    {
        $this->isphoneactivated = $isphoneactivated;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobileactivationcode()
    {
        return $this->mobileactivationcode;
    }

    /**
     * @param string $mobileactivationcode
     * @return Participant
     */
    public function setMobileactivationcode($mobileactivationcode)
    {
        $this->mobileactivationcode = $mobileactivationcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsactivated()
    {
        return $this->isactivated;
    }

    /**
     * @param string $isactivated
     * @return Participant
     */
    public function setIsactivated($isactivated)
    {
        $this->isactivated = $isactivated;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsmailingagreed()
    {
        return $this->ismailingagreed;
    }

    /**
     * @param string $ismailingagreed
     * @return Participant
     */
    public function setIsmailingagreed($ismailingagreed)
    {
        $this->ismailingagreed = $ismailingagreed;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsrulesagreed()
    {
        return $this->isrulesagreed;
    }

    /**
     * @param string $isrulesagreed
     * @return Participant
     */
    public function setIsrulesagreed($isrulesagreed)
    {
        $this->isrulesagreed = $isrulesagreed;

        return $this;
    }

    /**
     * @return string
     */
    public function getIspdagreed()
    {
        return $this->ispdagreed;
    }

    /**
     * @param string $ispdagreed
     * @return Participant
     */
    public function setIspdagreed($ispdagreed)
    {
        $this->ispdagreed = $ispdagreed;

        return $this;
    }

    /**
     * @return string
     */
    public function getMobilephone()
    {
        return $this->mobilephone;
    }

    /**
     * @param string $mobilephone
     * @return Participant
     */
    public function setMobilephone($mobilephone)
    {
        $this->mobilephone = $mobilephone;

        return $this;
    }

    /**
     * @return int
     */
    public function getVkId()
    {
        return $this->vk_id;
    }

    /**
     * @param int $vk_id
     * @return Participant
     */
    public function setVkId($vk_id)
    {
        $this->vk_id = $vk_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getOkId()
    {
        return $this->ok_id;
    }

    /**
     * @param int $ok_id
     * @return Participant
     */
    public function setOkId($ok_id)
    {
        $this->ok_id = $ok_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getFbId()
    {
        return $this->fb_id;
    }

    /**
     * @param int $fb_id
     * @return Participant
     */
    public function setFbId($fb_id)
    {
        $this->fb_id = $fb_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getGpId()
    {
        return $this->gp_id;
    }

    /**
     * @param int $gp_id
     * @return Participant
     */
    public function setGpId($gp_id)
    {
        $this->gp_id = $gp_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getTwId()
    {
        return $this->tw_id;
    }

    /**
     * @param int $tw_id
     * @return Participant
     */
    public function setTwId($tw_id)
    {
        $this->tw_id = $tw_id;

        return $this;
    }

    /**
     * @return int
     */
    public function getMailruId()
    {
        return $this->mailru_id;
    }

    /**
     * @param int $mailru_id
     * @return Participant
     */
    public function setMailruId($mailru_id)
    {
        $this->mailru_id = $mailru_id;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationdt()
    {
        return $this->registrationdt;
    }

    /**
     * @param \DateTime $registrationdt
     * @return Participant
     */
    public function setRegistrationdt($registrationdt)
    {
        $this->registrationdt = $registrationdt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getPhoneactivationdt()
    {
        return $this->phoneactivationdt;
    }

    /**
     * @param \DateTime $phoneactivationdt
     * @return Participant
     */
    public function setPhoneactivationdt($phoneactivationdt)
    {
        $this->phoneactivationdt = $phoneactivationdt;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getActivationdt()
    {
        return $this->activationdt;
    }

    /**
     * @param \DateTime $activationdt
     * @return Participant
     */
    public function setActivationdt($activationdt)
    {
        $this->activationdt = $activationdt;

        return $this;
    }


    /**
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     * @return Participant
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     * @return Participant
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * @return string
     */
    public function getIsemailactivated()
    {
        return $this->isemailactivated;
    }

    /**
     * @param string $isemailactivated
     * @return Participant
     */
    public function setIsemailactivated($isemailactivated)
    {
        $this->isemailactivated = $isemailactivated;

        return $this;
    }

    /**
     * @return string
     */
    public function getMailactivationcode()
    {
        return $this->mailactivationcode;
    }

    /**
     * @param string $mailactivationcode
     * @return Participant
     */
    public function setMailactivationcode($mailactivationcode)
    {
        $this->mailactivationcode = $mailactivationcode;

        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Participant
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    public function getRoles()
    {
        return $this->roles;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->email;
    }


    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof Participant) {
            return false;
        }

        if ($this->password !== $user->getPassword()) {
            return false;
        }

        if ($this->salt !== $user->getSalt()) {
            return false;
        }

        if ($this->email !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    /**
     * @return string
     */
    public function getIsmale()
    {
        return $this->ismale;
    }

    /**
     * @param string $ismale
     */
    public function setIsmale($ismale)
    {
        $this->ismale = $ismale;
    }

    /**
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param string $birthdate
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
    }

    public function getAge()
    {

        $birthDate = explode(".", $this->birthdate);

        $age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
            ? ((date("Y") - $birthDate[2]) - 1)
            : (date("Y") - $birthDate[2]));
        return $age;
    }

    /**
     * @param int $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }


    public function __toString()
    {
        return $this->email;
    }

    public function eraseCredentials()
    {
    }
}