<?php

namespace ADW\IlpCrmBundle\DependencyInjection\Security\Factory;

use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\Factory\SecurityFactoryInterface;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\DefinitionDecorator;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class IlpFactory
 * @package ADW\IlpCrmBundle\DependencyInjection\Security\Factory
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class IlpFactory implements SecurityFactoryInterface
{
    /**
     * @param ContainerBuilder $container
     * @param $id
     * @param $config
     * @param $userProvider
     * @param $defaultEntryPoint
     * @return array
     */
    public function create(ContainerBuilder $container, $id, $config, $userProvider, $defaultEntryPoint)
    {
        $providerId = 'security.authentication.provider.' . $id;
        $container->setDefinition($providerId, new DefinitionDecorator('adw_ilp.security.authentication.provider'))
                  ->replaceArgument(0, new Reference($userProvider));

        $listenerId = 'security.authentication.listener.' . $id;
        $container->setDefinition($listenerId, new DefinitionDecorator('adw_ilp.security.authentication.listener'));
        
        return array($providerId, $listenerId, $defaultEntryPoint);
    }

    /**
     * @return string
     */
    public function getPosition()
    {
        return 'pre_auth';
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return 'adw_ilp';
    }

    public function addConfiguration(NodeDefinition $node)
    {
    }
}