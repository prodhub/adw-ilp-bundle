<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 05/10/2017
 * Time: 13:04
 */

namespace ADW\IlpCrmBundle\DependencyInjection\Compiler;


use ADW\IlpCrmBundle\Security\Authentication\Provider\IlpSocialOAuthProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideServiceCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $definition = $container->getDefinition('hwi_oauth.authentication.provider.oauth');
        $definition->setClass(IlpSocialOAuthProvider::class);
    }


}