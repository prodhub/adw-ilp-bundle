<?php

namespace ADW\IlpCrmBundle\Method\CRM;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class GetProjectEventsListMethodDescription
 * @package ADW\IlpCrmBundle\Method\CRM
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetProjectEventsListMethodDescription extends AbstractMethodDescription
{
    /**
     * GetProjectEventsListMethodDescription constructor.
     */
    public function __construct()
    {
        $this->setOperationName('GetProjectEventsList');

        $option = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
        ];
        
        $this->setData($option);
    }
}