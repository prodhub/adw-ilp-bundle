<?php

namespace ADW\IlpCrmBundle\Method\CRM;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class RegisterEventMethodDescription
 * @package ADW\IlpCrmBundle\Method\CRM
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class RegisterEventMethodDescription extends AbstractMethodDescription
{
    /**
     * RegisterEventMethodDescription constructor.
     * @param $event
     * @param $status
     * @param $token
     * @param null $description
     */
    public function __construct($event, $status, $token, $description = null)
    {
        $this->setOperationName('RegisterEvent');

        $options = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
            'eventtype' => $event,
            'eventstatus' => $status,
            'token' => $token,
            'description' => $description,
        ];

        $this->setData($options);
    }
}