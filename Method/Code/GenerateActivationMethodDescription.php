<?php

namespace ADW\IlpCrmBundle\Method\Code;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\GenerateActivationCodeModel;
use ADW\IlpCrmBundle\Model\Request\ILPRequestBody;

/**
 * Class GenerateActivationMethodDescription
 * @package ADW\IlpCrmBundle\Method\Code
 */
class GenerateActivationMethodDescription extends AbstractMethodDescription
{
    private $login;

    /**
     * GenerateActivationMethodDescription constructor.
     * @param $login
     */
    public function __construct($login)
    {
        $this->login = $login;
    }

    /**
     * @param array $options
     * @return ILPRequestBody
     */
    public function getRequestData(array $options)
    {
        return new ILPRequestBody('GenerateActivationCode', [
            'login' => $this->login
        ]);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return GenerateActivationCodeModel::class;
    }

}