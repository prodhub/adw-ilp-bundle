<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\CheckLoginUniquenessModel;
use ADW\IlpCrmBundle\Model\Request\ILPRequestBody;

/**
 * Class CheckLoginUniquenessMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class CheckLoginUniquenessMethodDescription extends AbstractMethodDescription
{
    private $login;

    /**
     * CheckLoginUniquenessMethodDescription constructor.
     * @param $login
     */
    public function __construct($login)
    {
        $this->login = $login;
    }

    /**
     * @param array $options
     * @return ILPRequestBody
     */
    public function getRequestData(array $options)
    {
        return new ILPRequestBody('CheckLoginUniqueness', [
            'login' => $this->login
        ]);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return CheckLoginUniquenessModel::class;
    }

}