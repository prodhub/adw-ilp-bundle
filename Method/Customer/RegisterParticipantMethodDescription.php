<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\RegisterParticipantModel;

/**
 * Class RegisterParticipantMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class RegisterParticipantMethodDescription extends AbstractMethodDescription
{
    /**
     * RegisterParticipantMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('RegisterParticipant');
        $this->setData($option);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return RegisterParticipantModel::class;
    }
}