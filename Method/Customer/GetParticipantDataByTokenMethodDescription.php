<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\AuthorizedParticipantModel;

/**
 * Class GetParticipantDataByTokenMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetParticipantDataByTokenMethodDescription extends AbstractMethodDescription
{
    /**
     * GetParticipantDataByTokenMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('GetParticipantDataByToken');
        $this->setData($option);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return AuthorizedParticipantModel::class;
    }
}