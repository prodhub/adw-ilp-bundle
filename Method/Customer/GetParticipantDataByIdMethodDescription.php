<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\GetParticipantDataByIDModel;
Use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class GetParticipantDataByIdMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetParticipantDataByIdMethodDescription extends AbstractMethodDescription
{
    /**
     * GetParticipantDataByIdMethodDescription constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $this->setOperationName('GetParticipantDataById');

        $options = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
            'id' => $id
        ];
        $this->setData($options);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return GetParticipantDataByIdModel::class;
    }
}