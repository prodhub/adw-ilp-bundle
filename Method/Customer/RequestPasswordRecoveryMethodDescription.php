<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;

/**
 * Class RequestPasswordRecoveryMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class RequestPasswordRecoveryMethodDescription extends AbstractMethodDescription
{
    /**
     * RequestPasswordRecoveryMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('RequestPasswordRecovery');
        $this->setData($option);
    }
}