<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class FruitGardenMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class FruitGardenMethodDescription extends AbstractMethodDescription
{
    /**
     * FruitGardenMethodDescription constructor.
     * @param $prize
     * @param $token
     */
    public function __construct($prize, $token)
    {
        $this->setOperationName('RegisterEvent');

        $options = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
            'eventtype' => $prize,
            'token' => $token,
            'eventstatus' => 'BECAME_WINNER',
        ];

        $this->setData($options);
    }
}