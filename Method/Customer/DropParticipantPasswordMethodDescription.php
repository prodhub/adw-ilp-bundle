<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;

/**
 * Class DropParticipantPasswordMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class DropParticipantPasswordMethodDescription extends AbstractMethodDescription
{
    /**
     * DropParticipantPasswordMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('DropParticipantPassword');
        $this->setData($option);
    }
}