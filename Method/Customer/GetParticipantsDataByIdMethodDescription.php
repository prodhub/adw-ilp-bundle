<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\GetParticipantsDataByIdModel;
use ADW\IlpCrmBundle\Model\Request\ILPRequestBodyParticipants;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class GetParticipantsDataByIdMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetParticipantsDataByIdMethodDescription extends AbstractMethodDescription
{
    /**
     * GetParticipantsDataByIdMethodDescription constructor.
     * @param array $ids
     */
    public function __construct(array $ids)
    {
        $this->setOperationName('GetParticipantsDataById');

        $options = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
            'participants' => []
        ];

        foreach ($ids as $id) {
            $options['participants'][] = $id;
        }
        $this->setData($options);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return GetParticipantsDataByIdModel::class;
    }

    /**
     * @param array $options
     * @return ILPRequestBodyParticipants
     */
    public function getRequestData(array $options)
    {
        return new ILPRequestBodyParticipants($this->operation_name, $this->data);
    }
}