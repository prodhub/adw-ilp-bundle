<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\Model\Response\AuthorizationModel;

/**
 * Class AuthorizeParticipantMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class AuthorizeParticipantMethodDescription extends AbstractMethodDescription
{
    /**
     * AuthorizeParticipantMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('AuthorizeParticipant');
        $this->setData($option);
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return AuthorizationModel::class;
    }
}