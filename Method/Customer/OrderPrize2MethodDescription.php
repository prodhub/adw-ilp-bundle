<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class OrderPrize2MethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class OrderPrize2MethodDescription extends AbstractMethodDescription
{
    /**
     * OrderPrize2MethodDescription constructor.
     * @param $prize
     * @param $token
     */
    public function __construct($prize, $token)
    {
        $this->setOperationName('OrderPrize2');
        $options = [
            'channel' => ParamsDefinition::DEFAULT_CHANNEL,
            'prize_id' => $prize,
            'token' => $token,
        ];
        $this->setData($options);
    }
}