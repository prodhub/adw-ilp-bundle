<?php

namespace ADW\IlpCrmBundle\Method\Customer;

use ADW\IlpCrmBundle\Method\AbstractMethodDescription;

/**
 * Class UpdateParticipantMethodDescription
 * @package ADW\IlpCrmBundle\Method\Customer
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class UpdateParticipantMethodDescription extends AbstractMethodDescription
{
    /**
     * UpdateParticipantMethodDescription constructor.
     * @param array $option
     */
    public function __construct(array $option)
    {
        $this->setOperationName('UpdateParticipant');
        $this->setData($option);
    }
}