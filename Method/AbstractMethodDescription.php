<?php

namespace ADW\IlpCrmBundle\Method;

use ADW\IlpCrmBundle\Model\Request\ILPRequestBody;
use ADW\IlpCrmBundle\Model\Response\ILPResponseBody;
use ADW\RestClientBundle\Description\MethodDescriptionInterface;

/**
 * Class AbstractMethodDescription
 * @package ADW\IlpCrmBundle\Method
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
abstract class AbstractMethodDescription implements MethodDescriptionInterface
{
    /**
     * @var
     */
    protected $operation_name;

    /**
     * @var
     */
    protected $data;

    /**
     * @return mixed
     */
    public function getResponseDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_XML;
    }

    /**
     * @return mixed
     */
    public function getResponseDataModel()
    {
        return ILPResponseBody::class;
    }

    /**
     * @return array
     */
    public function getResponseDataContext()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getOptions()
    {
        return [];
    }

    /**
     * @return string
     */
    public function getResource()
    {
        return '';
    }

    /**
     * @param array $options
     * @return array
     */
    public function getQuery(array $options)
    {
        return [];
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        return 'POST';
    }

    /**
     * @param array $options
     * @return ILPRequestBody
     */
    public function getRequestData(array $options)
    {
        return new ILPRequestBody($this->operation_name, $this->data);
    }

    /**
     * @return mixed
     */
    public function getRequestDataFormat()
    {
        return MethodDescriptionInterface::FORMAT_XML;
    }

    /**
     * @return array
     */
    public function getRequestDataContext()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [];
    }

    /**
     * @return array
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param array $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return string
     */
    public function getOperationName()
    {
        return $this->operation_name;
    }

    /**
     * @param string $operation_name
     */
    public function setOperationName($operation_name)
    {
        $this->operation_name = $operation_name;
    }
}