<?php

namespace ADW\IlpCrmBundle;

/**
 * Class ParamsDefinition
 * @package ADW\IlpCrmBundle
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
final class ParamsDefinition
{
    const AUTH_ROUTE = 'ilp_crm.user.login';
    const EXIT_ROUTE = 'ilp_crm.user.logout';
    const REGISTER_ROUTE = 'ilp_crm.user.register';

    const DEFAULT_CHANNEL = 'S';
    const SOCIAL_CHANNEL = 'SOCIAL_ACCOUNT';

    const AUTH_ERROR_MESSAGE = 'Неправильный логин и (или) пароль';
}