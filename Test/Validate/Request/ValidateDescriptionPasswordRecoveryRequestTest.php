<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Request;

use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionPasswordRecoveryRequest;
use ADW\IlpCrmBundle\Model\Field\LoginField;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ValidateDescriptionPasswordRecoveryRequestTest extends TestCase
{
    /**
     * Test on success answer by request
     * @dataProvider getArrayValidData()
     * @param array $data
     */
    public function testDescriptionPasswordRecoveryRequestSuccess($data)
    {
        $initRequest = $this->getRequest($data);
        $description = new DescriptionPasswordRecoveryRequest($initRequest);

        $this->assertTrue($description->isSuccessStatus());
        $this->assertTrue(empty($description->getErrors()));
    }

    /**
     * Test on no-success answer by request if fields isn't valid
     * @dataProvider getArrayNotValidData()
     * @param array $data
     */
    public function testDescriptionPasswordRecoveryRequestNotValid($data)
    {
        $initRequest = $this->getRequest($data);
        $description = new DescriptionPasswordRecoveryRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());
        $this->assertTrue(!empty($description->getErrors()));

        $errors = $description->getErrors();

        $this->assertTrue($errors[LoginField::getName()] == LoginField::NOT_VALID_LOGIN, 'Get message is incorrect');
    }


    /**
     * Test on no-success answer by request if field is empty
     */
    public function testDescriptionPasswordRecoveryRequestEmpty()
    {
        $initRequest = $this->getRequest();
        $description = new DescriptionPasswordRecoveryRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());

        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $this->assertTrue($errors[LoginField::getName()] == LoginField::EMPTY_ERROR_MESSAGE, 'Get message is incorrect');
    }


    public function getArrayValidData()
    {
        return [
            [
                [
                    LoginField::getName() => 'test@mail.com',
                ]
            ],
            [
                [
                    LoginField::getName() => '70123456789',
                ]
            ],
        ];
    }

    public function getArrayNotValidData()
    {
        return [
                [
                    [
                        LoginField::getName() => 'no_valid_login',
                    ]
                ],
            ];
    }

    /**
     * @param array $data
     * @return Request
     */
    protected function getRequest($data = []){
        $request = new Request();
        foreach ($data as $fieldName => $value) {
            $request->query->set($fieldName, $value);
        }
        return $request;
    }



}
