<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Request;

use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionRegistrationRequest;
use ADW\IlpCrmBundle\Model\Field\DateField;
use ADW\IlpCrmBundle\Model\Field\EmailField;
use ADW\IlpCrmBundle\Model\Field\FirstNameField;
use ADW\IlpCrmBundle\Model\Field\IsMailingAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsMaleField;
use ADW\IlpCrmBundle\Model\Field\IsPdAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsRulesAgreedField;
use ADW\IlpCrmBundle\Model\Field\PasswordField;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ValidateDescriptionRegistrationRequestTest extends TestCase
{
    const NOT_VALID_VALUE = 'no_valid_value';
    const EMPTY_VALUE = 'empty_value';

    /**
     * Test on success answer by request
     */
    public function testDescriptionRegistrationRequestSuccess()
    {
        $initRequest = $this->getRequest();
        $description = new DescriptionRegistrationRequest($initRequest);

        $this->assertTrue($description->isSuccessStatus());
        $this->assertTrue(empty($description->getErrors()));
    }

    /**
     * Test on no-success answer by request if fields aren't valid
     */
    public function testDescriptionRegistrationRequestNoValid()
    {
        $initRequest = $this->getRequest(self::NOT_VALID_VALUE);
        $description = new DescriptionRegistrationRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());

        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $noValidMessages = [
            EmailField::getName() => EmailField::NOT_VALID_EMAIL,
            DateField::getName() => DateField::NOT_VALID_DATE,
            IsRulesAgreedField::getName() => IsRulesAgreedField::NOT_VALID_FLAG,
            IsPdAgreedField::getName() => IsPdAgreedField::NOT_VALID_FLAG,
            IsMailingAgreedField::getName() => IsMailingAgreedField::NOT_VALID_FLAG,
            IsMaleField::getName() => IsMaleField::NOT_VALID_FLAG,
        ];

        foreach ($errors as $index => $error) {
            $this->assertTrue($error == $noValidMessages[$index], 'Get message is incorrect');
        }
    }

    /**
     * Test on no-success answer by request if fields are empty
     */
    public function testDescriptionRegistrationRequestEmpty()
    {
        $initRequest = $this->getRequest(self::EMPTY_VALUE);
        $description = new DescriptionRegistrationRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());

        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $noValidMessages = [
            EmailField::getName() => EmailField::EMPTY_ERROR_MESSAGE,
            DateField::getName() => DateField::EMPTY_ERROR_MESSAGE,
            PasswordField::getName() => PasswordField::EMPTY_ERROR_MESSAGE,
            FirstNameField::getName() => FirstNameField::EMPTY_ERROR_MESSAGE,
            IsRulesAgreedField::getName() => IsRulesAgreedField::NOT_VALID_FLAG,
            IsPdAgreedField::getName() => IsPdAgreedField::NOT_VALID_FLAG,
            IsMailingAgreedField::getName() => IsMailingAgreedField::NOT_VALID_FLAG,
            IsMaleField::getName() => IsMaleField::NOT_VALID_FLAG,
        ];

        foreach ($errors as $index => $error) {
            $this->assertTrue($error == $noValidMessages[$index], 'Get message is incorrect');
        }
    }

    /**
     * @return Request
     */
    protected function getRequest($type = ''){
        switch ($type){
            case self::NOT_VALID_VALUE:
                $fields = $this->getNoValidFields();
                break;

            case self::EMPTY_VALUE:
                $fields = $this->getEmptyFields();
                break;

            default:
                $fields = $this->getFields();
        }
        $request = new Request();
        foreach ($fields as $index => $field) {
            $request->query->set($index, $field);
        }

        return $request;
    }

    /**
     * @return array
     */
    protected function getFields()
    {
        return [
            "email" => 'test@gmail.com',
            "password" => 'qwe123',
            "isrulesagreed" => 'Y',
            "ispdagreed" => 'Y',
            "ismailingagreed" => 'Y',
            "firstname" => 'Иван',
            "lastname" => 'Иванов',
            "birthdate" => '11.11.2001',
            "ismale" => 'Y',
            "city" => 'Moscow',
        ];
    }

    protected function getNoValidFields()
    {
        return [
            "email" => 'No-valid-value',
            "isrulesagreed" => 'No-valid-value',
            "ispdagreed" => 'No-valid-value',
            "ismailingagreed" => 'No-valid-value',
            "firstname" => 'Иван',
            "lastname" => 'Иванов',
            "birthdate" => 'No-valid-value',
            "ismale" => 'No-valid-value',
            "password" => 'qwe123',
            "city" => 'Moscow',
        ];
    }

    protected function getEmptyFields()
    {
        return [
            "email" => '',
            "isrulesagreed" => '',
            "ispdagreed" => '',
            "ismailingagreed" => '',
            "firstname" => '',
            "lastname" => '',
            "birthdate" => '',
            "ismale" => '',
            "password" => '',
            "city" => 'Moscow',
        ];
    }
}
