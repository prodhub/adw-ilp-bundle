<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Request;

use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionCheckLoginRequest;
use ADW\IlpCrmBundle\Model\Field\LoginField;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ValidateDescriptionCheckLoginRequestTest extends TestCase
{
    /**
     * Test on success answer by request
     * @dataProvider getArrayValidLogin()
     * @param string $login
     */
    public function testDescriptionRegistrationRequestSuccess($login)
    {
        $initRequest = $this->getRequest($login);
        $description = new DescriptionCheckLoginRequest($initRequest);

        $this->assertTrue($description->isSuccessStatus());
        $this->assertTrue(empty($description->getErrors()));
    }

    /**
     * Test on no-success answer by request if fields isn't valid
     * Если переданное занчение соответствует хоть одной из объявленных
     * в доке маскок
     * 7DEFXXXXXXX /  test@test.ts / vk 123  / ok 123 /  fb 123 /  gp 123
     * вернем true
     * @dataProvider getNotArrayValidLogin()
     * @param string $login
     */
    public function testDescriptionRegistrationRequestNotValid($login)
    {
        $initRequest = $this->getRequest($login);
        $description = new DescriptionCheckLoginRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());
        $this->assertTrue(!empty($description->getErrors()));


        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $this->assertTrue($errors[LoginField::getName()] == LoginField::NOT_VALID_LOGIN, 'Get message is incorrect');
    }



    /**
     * Test on no-success answer by request if field is empty
     */
    public function testDescriptionRegistrationRequestEmpty()
    {
        $initRequest = $this->getRequest();
        $description = new DescriptionCheckLoginRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());

        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $this->assertTrue($errors[LoginField::getName()] == LoginField::EMPTY_ERROR_MESSAGE, 'Get message is incorrect');
    }


    public function getArrayValidLogin()
    {
        return [
            ['test@mail.com'],
            ['70123456789'],
            ['vk 123sdasdas887'],
            ['ok 123asd77777'],
            ['fb 123'],
            ['gp 123as323423']
        ];
    }

    public function getNotArrayValidLogin()
    {
        return [
            ['not-valid@mail'],
            ['7012345678911'],
            ['v123sdasdas887'],
            ['ok123asd77777'],
            ['fbb 123'],
            ['11gp123as323423']
        ];
    }

    /**
     * @param string $login
     * @return Request
     */
    protected function getRequest($login = ''){
        $request = new Request();
        $request->query->set(LoginField::getName(), $login);
        return $request;
    }



}
