<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Request;

use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionDropParticipantPasswordRequest;
use ADW\IlpCrmBundle\Model\Field\ForcedPasswordField;
use ADW\IlpCrmBundle\Model\Field\IsSecureField;
use ADW\IlpCrmBundle\Model\Field\LoginField;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request;

class ValidateDescriptionDropParticipantPasswordRequestTest extends TestCase
{
    /**
     * Test on success answer by request
     * @dataProvider getArrayValidData()
     * @param array $data
     */
    public function testDescriptionDropParticipantPasswordRequestSuccess($data)
    {
        $initRequest = $this->getRequest($data);
        $description = new DescriptionDropParticipantPasswordRequest($initRequest);

        $this->assertTrue($description->isSuccessStatus());
        $this->assertTrue(empty($description->getErrors()));
    }

    /**
     * Test on no-success answer by request if fields isn't valid
     * @dataProvider getArrayNotValidData()
     * @param array $data
     */
    public function testDescriptionDropParticipantPasswordRequestNotValid($data)
    {
        $initRequest = $this->getRequest($data);
        $description = new DescriptionDropParticipantPasswordRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());
        $this->assertTrue(!empty($description->getErrors()));

        $errors = $description->getErrors();

        $this->assertTrue($errors[LoginField::getName()] == LoginField::NOT_VALID_LOGIN, 'Get message is incorrect');
        $this->assertTrue($errors[IsSecureField::getName()] == IsSecureField::NOT_VALID_FLAG, 'Get message is incorrect');
    }


    /**
     * Test on no-success answer by request if field is empty
     */
    public function testDescriptionDropParticipantPasswordRequestEmpty()
    {
        $initRequest = $this->getRequest();
        $description = new DescriptionDropParticipantPasswordRequest($initRequest);

        $this->assertFalse($description->isSuccessStatus());

        $errors = $description->getErrors();
        $this->assertTrue(!empty($errors));

        $this->assertTrue($errors[LoginField::getName()] == LoginField::EMPTY_ERROR_MESSAGE, 'Get message is incorrect');
    }


    public function getArrayValidData()
    {
        return [
            [
                [
                    LoginField::getName() => 'test@mail.com',
                    ForcedPasswordField::getName() => 'pass1111',
                    IsSecureField::getName() => 'Y'
                ]
            ],
            [
                [
                    LoginField::getName() => '70123456789',
                    ForcedPasswordField::getName() => 'pass1111',
                    IsSecureField::getName() => 'N'
                ]
            ],
        ];
    }

    public function getArrayNotValidData()
    {
        return [
                [
                    [
                        LoginField::getName() => 'no_valid_login',
                        ForcedPasswordField::getName() => 'pass1111',
                        IsSecureField::getName() => 'no_valid'
                    ]
                ],
            ];
    }

    /**
     * @param array $data
     * @return Request
     */
    protected function getRequest($data = []){
        $request = new Request();
        foreach ($data as $fieldName => $value) {
            $request->query->set($fieldName, $value);
        }
        return $request;
    }



}
