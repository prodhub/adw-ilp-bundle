<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Response;

use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionCheckLoginResponse;
use ADW\IlpCrmBundle\Model\Response\CheckLoginUniquenessModel;
use PHPUnit\Framework\TestCase;

class ValidateDescriptionCheckLoginnResponseTest extends TestCase
{

    /**
     * Test on success answer by request
     */
    public function testDescriptionCheckLoginResponseSuccess()
    {
        $checkLoginObj = $this->getMock(CheckLoginUniquenessModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(1);

        /** @var DescriptionCheckLoginResponse $checkLoginObj */
        $responseDescription = new DescriptionCheckLoginResponse($checkLoginObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }


    /**
     * Test error checking
     */
    public function testDescriptionCheckLoginResponseErrorRegister()
    {
        $checkLoginObj = $this->getMock(CheckLoginUniquenessModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(2);

        /** @var DescriptionCheckLoginResponse $checkLoginObj */
        $responseDescription = new DescriptionCheckLoginResponse($checkLoginObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionCheckLoginResponse::ERROR_LOGIN_IS_NOT_UNIQUE);
    }


    /**
     * Test Unknown Error
     */
    public function testDescriptionCheckLoginResponseUnknownError()
    {
        $checkLoginObj = $this->getMock(CheckLoginUniquenessModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(3);

        /** @var DescriptionCheckLoginResponse $checkLoginObj */
        $responseDescription = new DescriptionCheckLoginResponse($checkLoginObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionCheckLoginResponse::ERROR_UNKNOWN_ANSWER);
    }
}
