<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Response;

use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionGenerateActivationCodeResponse;
use ADW\IlpCrmBundle\Model\Response\GenerateActivationCodeModel;
use PHPUnit\Framework\TestCase;

class ValidateDescriptionGenerateActivationCodeResponseTest extends TestCase
{

    /**
     * Test on success answer by request
     */
    public function testDescriptionGenerateActivationCodeByPhoneResponseSuccess()
    {
        $checkLoginObj = $this->getMock(GenerateActivationCodeModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(1);

        /** @var DescriptionGenerateActivationCodeResponse $checkLoginObj */
        $responseDescription = new DescriptionGenerateActivationCodeResponse($checkLoginObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }

    /**
     * Test on success answer by request
     */
    public function testDescriptionGenerateActivationByEmailCodeResponseSuccess()
    {
        $checkLoginObj = $this->getMock(GenerateActivationCodeModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(2);

        /** @var DescriptionGenerateActivationCodeResponse $checkLoginObj */
        $responseDescription = new DescriptionGenerateActivationCodeResponse($checkLoginObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }


    /**
     * Test error checking
     */
    public function testDescriptionGenerateActivationByPhoneCodeResponseErrorRegister()
    {
        $checkLoginObj = $this->getMock(GenerateActivationCodeModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(3);

        /** @var DescriptionGenerateActivationCodeResponse $checkLoginObj */
        $responseDescription = new DescriptionGenerateActivationCodeResponse($checkLoginObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionGenerateActivationCodeResponse::ERROR_PHONE_ALREADY_ACTIVE);
    }


    /**
     * Test error checking
     */
    public function testDescriptionGenerateActivationByEmailCodeResponseErrorRegister()
    {
        $checkLoginObj = $this->getMock(GenerateActivationCodeModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(4);

        /** @var DescriptionGenerateActivationCodeResponse $checkLoginObj */
        $responseDescription = new DescriptionGenerateActivationCodeResponse($checkLoginObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionGenerateActivationCodeResponse::ERROR_EMAIL_ALREADY_ACTIVE);
    }

    /**
     * Test Unknown Error
     */
    public function testDescriptionGenerateActivationCodeResponseUnknownError()
    {
        $checkLoginObj = $this->getMock(GenerateActivationCodeModel::class);
        $checkLoginObj->expects($this->any())
            ->method('getResult')
            ->willReturn(5);

        /** @var DescriptionGenerateActivationCodeResponse $checkLoginObj */
        $responseDescription = new DescriptionGenerateActivationCodeResponse($checkLoginObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionGenerateActivationCodeResponse::ERROR_UNKNOWN_ANSWER);
    }
}
