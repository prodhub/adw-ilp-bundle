<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Response;

use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionDropParticipantPasswordResponse;
use ADW\IlpCrmBundle\Model\Response\ILPResponseBody;
use PHPUnit\Framework\TestCase;

class ValidateDescriptionDropParticipantPasswordResponseTest extends TestCase
{

    /**
     * Test on success answer by request
     */
    public function testDescriptionDropParticipantPasswordResponseSuccess()
    {
        $updPasswordObj = $this->getMock(ILPResponseBody::class);
        $updPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(1);

        /** @var ILPResponseBody $updPasswordObj */
        $responseDescription = new DescriptionDropParticipantPasswordResponse($updPasswordObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }


    /**
     * Test error checking
     */
    public function testDescriptionDropParticipantPasswordResponseErrorRegister()
    {
        $updPasswordObj = $this->getMock(ILPResponseBody::class);
        $updPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(2);

        /** @var ILPResponseBody $updPasswordObj */
        $responseDescription = new DescriptionDropParticipantPasswordResponse($updPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionDropParticipantPasswordResponse::ERROR_PARTICIPANT_NOT_FOUND);
    }


    /**
     * Test Unknown Error
     */
    public function testDescriptionDropParticipantPasswordResponseUnknownError()
    {
        $updPasswordObj = $this->getMock(ILPResponseBody::class);
        $updPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(3);

        /** @var ILPResponseBody $updPasswordObj */
        $responseDescription = new DescriptionDropParticipantPasswordResponse($updPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionDropParticipantPasswordResponse::ERROR_UNKNOWN_ERROR);
    }

    /**
     * Test Unknown Answer
     */
    public function testDescriptionDropParticipantPasswordResponseUnknownAnswer()
    {
        $updPasswordObj = $this->getMock(ILPResponseBody::class);
        $updPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(5);

        /** @var ILPResponseBody $updPasswordObj */
        $responseDescription = new DescriptionDropParticipantPasswordResponse($updPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionDropParticipantPasswordResponse::ERROR_UNKNOWN_ANSWER);
    }
}
