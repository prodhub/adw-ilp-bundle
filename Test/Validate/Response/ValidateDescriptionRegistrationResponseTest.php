<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Response;

use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionRegistrationResponse;
use ADW\IlpCrmBundle\Model\Response\RegisterParticipantModel;
use PHPUnit\Framework\TestCase;

class ValidateDescriptionRegistrationResponseTest extends TestCase
{

    /**
     * Test on success answer by request
     */
    public function testDescriptionRegistrationResponseSuccess()
    {
        $registerObj = $this->getMock(RegisterParticipantModel::class);
        $registerObj->expects($this->any())
            ->method('getResult')
            ->willReturn(1);

        /** @var DescriptionRegistrationResponse $registerObj */
        $responseDescription = new DescriptionRegistrationResponse($registerObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }


    /**
     * Test error register
     */
    public function testDescriptionRegistrationResponseErrorRegister()
    {
        $registerObj = $this->getMock(RegisterParticipantModel::class);
        $registerObj->expects($this->any())
            ->method('getResult')
            ->willReturn(2);

        /** @var DescriptionRegistrationResponse $registerObj */
        $responseDescription = new DescriptionRegistrationResponse($registerObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionRegistrationResponse::ERROR_REGISTRATION);
    }


    /**
     * Test user exists
     */
    public function testDescriptionRegistrationResponseErrorUserExist()
    {
        $registerObj = $this->getMock(RegisterParticipantModel::class);
        $registerObj->expects($this->any())
            ->method('getResult')
            ->willReturn(3);

        /** @var DescriptionRegistrationResponse $registerObj */
        $responseDescription = new DescriptionRegistrationResponse($registerObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionRegistrationResponse::ERROR_REGISTRATION_USER_EXISTS);
    }

    /**
     * Test Unknown Error
     */
    public function testDescriptionRegistrationResponseUnknownError()
    {
        $registerObj = $this->getMock(RegisterParticipantModel::class);
        $registerObj->expects($this->any())
            ->method('getResult')
            ->willReturn(4);

        /** @var DescriptionRegistrationResponse $registerObj */
        $responseDescription = new DescriptionRegistrationResponse($registerObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionRegistrationResponse::ERROR_UNKNOWN_ANSWER);
    }
}
