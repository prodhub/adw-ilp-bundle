<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 27/09/2017
 * Time: 14:13
 */

namespace ADW\IlpCrmBundle\Test\Validate\Response;

use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionPasswordRecoveryResponse;
use ADW\IlpCrmBundle\Model\Response\ILPResponseBody;
use PHPUnit\Framework\TestCase;

class ValidateDescriptionPasswordRecoveryResponseTest extends TestCase
{

    /**
     * Test on success answer by request
     */
    public function testDescriptionPasswordRecoveryResponseSuccess()
    {
        $recoveryPasswordObj = $this->getMock(ILPResponseBody::class);
        $recoveryPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(1);

        /** @var ILPResponseBody $recoveryPasswordObj */
        $responseDescription = new DescriptionPasswordRecoveryResponse($recoveryPasswordObj);
        $this->assertTrue($responseDescription->getStatus());
        $this->assertTrue(empty($responseDescription->getError()));
    }


    /**
     * Test error checking
     */
    public function testDescriptionPasswordRecoveryResponseErrorRegister()
    {
        $recoveryPasswordObj = $this->getMock(ILPResponseBody::class);
        $recoveryPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(2);

        /** @var ILPResponseBody $recoveryPasswordObj */
        $responseDescription = new DescriptionPasswordRecoveryResponse($recoveryPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionPasswordRecoveryResponse::ERROR_PARTICIPANT_NOT_FOUND);
    }


    /**
     * Test Unknown Error
     */
    public function testDescriptionPasswordRecoveryResponseUnknownError()
    {
        $recoveryPasswordObj = $this->getMock(ILPResponseBody::class);
        $recoveryPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(3);

        /** @var ILPResponseBody $recoveryPasswordObj */
        $responseDescription = new DescriptionPasswordRecoveryResponse($recoveryPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionPasswordRecoveryResponse::ERROR_PROCESSING_REQUEST);
    }

    /**
     * Test Unknown Answer
     */
    public function testDescriptionPasswordRecoveryResponseUnknownAnswer()
    {
        $recoveryPasswordObj = $this->getMock(ILPResponseBody::class);
        $recoveryPasswordObj->expects($this->any())
            ->method('getResult')
            ->willReturn(5);

        /** @var ILPResponseBody $recoveryPasswordObj */
        $responseDescription = new DescriptionPasswordRecoveryResponse($recoveryPasswordObj);
        $this->assertFalse($responseDescription->getStatus());
        $this->assertTrue(!empty($responseDescription->getError()));
        $this->assertEquals($responseDescription->getError(), DescriptionPasswordRecoveryResponse::ERROR_UNKNOWN_ANSWER);
    }
}
