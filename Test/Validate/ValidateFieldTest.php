<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 16:40
 */

namespace ADW\IlpCrmBundle\Test\Validate;

use ADW\IlpCrmBundle\Model\Field\ForcedPasswordField;
use ADW\IlpCrmBundle\Model\Field\IsSecureField;
use PHPUnit\Framework\TestCase;


use ADW\IlpCrmBundle\Model\Field\ChannelField;
use ADW\IlpCrmBundle\Model\Field\CityField;
use ADW\IlpCrmBundle\Model\Field\DateField;
use ADW\IlpCrmBundle\Model\Field\EmailField;
use ADW\IlpCrmBundle\Model\Field\FirstNameField;
use ADW\IlpCrmBundle\Model\Field\IsMailingAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsMaleField;
use ADW\IlpCrmBundle\Model\Field\IsPdAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsRulesAgreedField;
use ADW\IlpCrmBundle\Model\Field\LastNameField;
use ADW\IlpCrmBundle\Model\Field\PasswordField;
use ADW\IlpCrmBundle\Model\Field\Social\FbField;
use ADW\IlpCrmBundle\Model\Field\Social\OkField;
use ADW\IlpCrmBundle\Model\Field\Social\VkField;
use ADW\IlpCrmBundle\Model\Field\TokenField;

/**
 * Tests for checking validation layer
 */

class ValidateFieldTest extends TestCase
{
    const ERROR_MESSAGE = 'Validate incorrect work';

    public function testEmailField()
    {
        $emailField = new EmailField('', true);
        $this->assertFalse($emailField->isValidateValue());
        $this->assertEquals(EmailField::EMPTY_ERROR_MESSAGE, $emailField->getError(),self::ERROR_MESSAGE);

        $emailField = new EmailField('');
        $this->assertFalse($emailField->isValidateValue());
        $this->assertEquals(EmailField::NOT_VALID_EMAIL, $emailField->getError(),self::ERROR_MESSAGE);

        $emailField = new EmailField('eeeeeee@sd', true);
        $this->assertFalse($emailField->isValidateValue());
        $this->assertEquals(EmailField::NOT_VALID_EMAIL, $emailField->getError(),self::ERROR_MESSAGE);

        $emailField = new EmailField('test@mail.com', true);
        $this->assertTrue($emailField->isValidateValue());
        $this->assertTrue(empty($emailField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testChannelField()
    {
        $channelField = new ChannelField('', true);
        $this->assertFalse($channelField->isValidateValue());
        $this->assertEquals(ChannelField::EMPTY_ERROR_MESSAGE, $channelField->getError(),self::ERROR_MESSAGE);

        $channelField = new ChannelField('');
        $this->assertFalse($channelField->isValidateValue());
        $this->assertEquals(ChannelField::NOT_VALID_CHANNEL, $channelField->getError(),self::ERROR_MESSAGE);

        $channelField = new ChannelField('NOT_VALID_CHANNEL', true);
        $this->assertFalse($channelField->isValidateValue());
        $this->assertEquals(ChannelField::NOT_VALID_CHANNEL, $channelField->getError(),self::ERROR_MESSAGE);

        $channelField = new ChannelField('S', true);
        $this->assertTrue($channelField->isValidateValue());
        $this->assertTrue(empty($channelField->getError()) ,self::ERROR_MESSAGE);
    }


    public function testCityField()
    {
        $cityField = new CityField('', true);
        $this->assertFalse($cityField->isValidateValue());
        $this->assertEquals(CityField::EMPTY_ERROR_MESSAGE, $cityField->getError(),self::ERROR_MESSAGE);

        $cityField = new CityField('Москва', true);
        $this->assertTrue($cityField->isValidateValue());
        $this->assertTrue(empty($cityField->getError()) ,self::ERROR_MESSAGE);
    }

    public function testDateField()
    {
        $dateField = new DateField('', true);
        $this->assertFalse($dateField->isValidateValue());
        $this->assertEquals(DateField::EMPTY_ERROR_MESSAGE, $dateField->getError(),self::ERROR_MESSAGE);

        $dateField = new DateField('');
        $this->assertFalse($dateField->isValidateValue());
        $this->assertEquals(DateField::NOT_VALID_DATE, $dateField->getError(),self::ERROR_MESSAGE);

        $dateField = new DateField('34.34.34', true);
        $this->assertFalse($dateField->isValidateValue());
        $this->assertEquals(DateField::NOT_VALID_DATE, $dateField->getError(),self::ERROR_MESSAGE);

        $dateField = new DateField('12.12.2010', true);
        $this->assertTrue($dateField->isValidateValue());
        $this->assertTrue(empty($dateField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testFirstNameField()
    {
        $firstNameField = new FirstNameField('', true);
        $this->assertFalse($firstNameField->isValidateValue());
        $this->assertEquals(FirstNameField::EMPTY_ERROR_MESSAGE, $firstNameField->getError(),self::ERROR_MESSAGE);

        $firstNameField = new FirstNameField('Иван', true);
        $this->assertTrue($firstNameField->isValidateValue());
        $this->assertTrue(empty($firstNameField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testIsMailingAgreedField()
    {
        $isMailingAgreedField = new IsMailingAgreedField('', true);
        $this->assertFalse($isMailingAgreedField->isValidateValue());
        $this->assertEquals(IsMailingAgreedField::EMPTY_ERROR_MESSAGE, $isMailingAgreedField->getError(),self::ERROR_MESSAGE);

        $isMailingAgreedField = new IsMailingAgreedField('');
        $this->assertTrue($isMailingAgreedField->isValidateValue());
        $this->assertEquals('', $isMailingAgreedField->getError(),self::ERROR_MESSAGE);

        $isMailingAgreedField = new IsMailingAgreedField('F', true);
        $this->assertFalse($isMailingAgreedField->isValidateValue());
        $this->assertEquals(IsMailingAgreedField::NOT_VALID_FLAG, $isMailingAgreedField->getError(),self::ERROR_MESSAGE);

        $isMailingAgreedField = new IsMailingAgreedField('N', true);
        $this->assertTrue($isMailingAgreedField->isValidateValue());
        $this->assertTrue(empty($isMailingAgreedField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testIsMaleField()
    {
        $isMaleField = new IsMaleField('', true);
        $this->assertFalse($isMaleField->isValidateValue());
        $this->assertEquals(IsMaleField::EMPTY_ERROR_MESSAGE, $isMaleField->getError(),self::ERROR_MESSAGE);

        $isMaleField = new IsMaleField('');
        $this->assertTrue($isMaleField->isValidateValue());
        $this->assertEquals('', $isMaleField->getError(),self::ERROR_MESSAGE);

        $isMaleField = new IsMaleField('F', true);
        $this->assertFalse($isMaleField->isValidateValue());
        $this->assertEquals(IsMaleField::NOT_VALID_FLAG, $isMaleField->getError(),self::ERROR_MESSAGE);

        $isMaleField = new IsMaleField('N', true);
        $this->assertTrue($isMaleField->isValidateValue());
        $this->assertTrue(empty($isMaleField->getError()) ,self::ERROR_MESSAGE);

    }


    public function testIsPdAgreedField()
    {
        $isPdAgreedField = new IsPdAgreedField('', true);
        $this->assertFalse($isPdAgreedField->isValidateValue());
        $this->assertEquals(IsPdAgreedField::EMPTY_ERROR_MESSAGE, $isPdAgreedField->getError(),self::ERROR_MESSAGE);

        $isPdAgreedField = new IsPdAgreedField('');
        $this->assertTrue($isPdAgreedField->isValidateValue());
        $this->assertEquals('', $isPdAgreedField->getError(),self::ERROR_MESSAGE);

        $isPdAgreedField = new IsPdAgreedField('F', true);
        $this->assertFalse($isPdAgreedField->isValidateValue());
        $this->assertEquals(IsPdAgreedField::NOT_VALID_FLAG, $isPdAgreedField->getError(),self::ERROR_MESSAGE);

        $isPdAgreedField = new IsPdAgreedField('N', true);
        $this->assertTrue($isPdAgreedField->isValidateValue());
        $this->assertTrue(empty($isPdAgreedField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testIsRulesAgreedField()
    {
        $isRulesAgreedField = new IsRulesAgreedField('', true);
        $this->assertFalse($isRulesAgreedField->isValidateValue());
        $this->assertEquals(IsRulesAgreedField::EMPTY_ERROR_MESSAGE, $isRulesAgreedField->getError(),self::ERROR_MESSAGE);

        $isRulesAgreedField = new IsRulesAgreedField('');
        $this->assertTrue($isRulesAgreedField->isValidateValue());
        $this->assertEquals('', $isRulesAgreedField->getError(),self::ERROR_MESSAGE);

        $isRulesAgreedField = new IsRulesAgreedField('F', true);
        $this->assertFalse($isRulesAgreedField->isValidateValue());
        $this->assertEquals(IsRulesAgreedField::NOT_VALID_FLAG, $isRulesAgreedField->getError(),self::ERROR_MESSAGE);

        $isRulesAgreedField = new IsRulesAgreedField('N', true);
        $this->assertTrue($isRulesAgreedField->isValidateValue());
        $this->assertTrue(empty($isRulesAgreedField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testIsSecureField()
    {
        $isSecureField = new IsSecureField('', true);
        $this->assertFalse($isSecureField->isValidateValue());
        $this->assertEquals(IsSecureField::EMPTY_ERROR_MESSAGE, $isSecureField->getError(),self::ERROR_MESSAGE);

        $isSecureField = new IsSecureField('');
        $this->assertTrue($isSecureField->isValidateValue());
        $this->assertEquals('', $isSecureField->getError(),self::ERROR_MESSAGE);

        $isSecureField = new IsSecureField('F', true);
        $this->assertFalse($isSecureField->isValidateValue());
        $this->assertEquals(IsSecureField::NOT_VALID_FLAG, $isSecureField->getError(),self::ERROR_MESSAGE);

        $isSecureField = new IsSecureField('N', true);
        $this->assertTrue($isSecureField->isValidateValue());
        $this->assertTrue(empty($isSecureField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testLastNameField()
    {
        $lastNameField = new LastNameField('', true);
        $this->assertFalse($lastNameField->isValidateValue());
        $this->assertEquals(LastNameField::EMPTY_ERROR_MESSAGE, $lastNameField->getError(),self::ERROR_MESSAGE);

        $lastNameField = new LastNameField('Иванов', true);
        $this->assertTrue($lastNameField->isValidateValue());
        $this->assertTrue(empty($lastNameField->getError()) ,self::ERROR_MESSAGE);

    }


    public function testPasswordField()
    {
        $passwordField = new PasswordField('', true);
        $this->assertFalse($passwordField->isValidateValue());
        $this->assertEquals(PasswordField::EMPTY_ERROR_MESSAGE, $passwordField->getError(),self::ERROR_MESSAGE);

        $passwordField = new PasswordField('Иванов', true);
        $this->assertTrue($passwordField->isValidateValue());
        $this->assertTrue(empty($passwordField->getError()) ,self::ERROR_MESSAGE);

    }
    public function testForcedPasswordField()
    {
        $passwordField = new ForcedPasswordField('', true);
        $this->assertFalse($passwordField->isValidateValue());
        $this->assertEquals(PasswordField::EMPTY_ERROR_MESSAGE, $passwordField->getError(),self::ERROR_MESSAGE);

        $passwordField = new ForcedPasswordField('Иванов', true);
        $this->assertTrue($passwordField->isValidateValue());
        $this->assertTrue(empty($passwordField->getError()) ,self::ERROR_MESSAGE);

    }


    public function testTokenField()
    {
        $tokenField = new TokenField('', true);
        $this->assertFalse($tokenField->isValidateValue());
        $this->assertEquals(TokenField::EMPTY_ERROR_MESSAGE, $tokenField->getError(),self::ERROR_MESSAGE);

        $tokenField = new TokenField('Иванов', true);
        $this->assertTrue($tokenField->isValidateValue());
        $this->assertTrue(empty($tokenField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testFbField()
    {
        $fbField = new FbField('', true);
        $this->assertFalse($fbField->isValidateValue());
        $this->assertEquals(FbField::EMPTY_ERROR_MESSAGE, $fbField->getError(),self::ERROR_MESSAGE);

        $fbField = new FbField('vk 111111', true);
        $this->assertTrue($fbField->isValidateValue());
        $this->assertTrue(empty($fbField->getError()) ,self::ERROR_MESSAGE);

    }

    public function testVkField()
    {
        $vkField = new VkField('', true);
        $this->assertFalse($vkField->isValidateValue());
        $this->assertEquals(VkField::EMPTY_ERROR_MESSAGE, $vkField->getError(),self::ERROR_MESSAGE);

        $vkField = new VkField('fb 22222', true);
        $this->assertTrue($vkField->isValidateValue());
        $this->assertTrue(empty($vkField->getError()) ,self::ERROR_MESSAGE);
    }

    public function testOkField()
    {
        $okField = new OkField('', true);
        $this->assertFalse($okField->isValidateValue());
        $this->assertEquals(OkField::EMPTY_ERROR_MESSAGE, $okField->getError(),self::ERROR_MESSAGE);

        $okField = new OkField('ok 33333', true);
        $this->assertTrue($okField->isValidateValue());
        $this->assertTrue(empty($okField->getError()) ,self::ERROR_MESSAGE);
    }
}