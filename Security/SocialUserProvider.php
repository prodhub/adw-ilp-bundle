<?php

namespace ADW\IlpCrmBundle\Security;

use ADW\IlpCrmBundle\Entity\Participant;
use ADW\IlpCrmBundle\Method\Customer\AuthorizeParticipantMethodDescription;
use ADW\IlpCrmBundle\Method\Customer\RegisterParticipantMethodDescription;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionAuthorizeParticipantResponse;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionRegistrationResponse;
use ADW\IlpCrmBundle\Model\DescriptionSocialResponse\BaseDescriptionSocialResponse;
use ADW\IlpCrmBundle\Model\DescriptionSocialResponse\FactoryDescriptionSocialResponse;
use ADW\IlpCrmBundle\Model\Response\ILPResponseBody;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use ADW\IlpCrmBundle\Service\RestClientService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthUserProvider;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class SocialUserProvider
 * @package ADW\IlpCrmBundle\Security
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class SocialUserProvider extends OAuthUserProvider
{
    use UserProviderTrait;

    /**
     * @var Registry
     */
    protected $doctrine;
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var RestClientService
     */
    protected $crmClient;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var IlpAuthUserToken
     */
    protected $authenticatedToken;


    /**
     * ILPUserProvider constructor.
     * @param Registry $doctrine
     * @param TokenStorageInterface $tokenStorage
     * @param RestClientService $crmClient
     * @param ContainerInterface $container
     */
    public function __construct(Registry $doctrine, TokenStorageInterface $tokenStorage, $crmClient, $container) {
        $this->doctrine = $doctrine;
        $this->tokenStorage = $tokenStorage;
        $this->crmClient = $crmClient;
        $this->container = $container;
    }

    /**
     * {@inheritDoc}
     */
    public function supportsClass($class)
    {
        return $class === Participant::class;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByOAuthUserResponse(UserResponseInterface $response)
    {
        return $this->loadUserByResponse($response);
    }

    /**
     * @param UserResponseInterface $response
     * @return Participant|null
     */
    public function loadUserByResponse(UserResponseInterface $response)
    {
        $user = null;

        /**
         * Получем двухбуквенный код социальной сети и ID пользователя,
         * которые необходимы для отправки на сторону ILP с целью получения данных о пользователе.
         */

        $factoryResponse  = new FactoryDescriptionSocialResponse($response);
        $descriptionSocialResponse = $factoryResponse->getResponseDescription();

        if($descriptionSocialResponse instanceof BaseDescriptionSocialResponse){
            //Переносим в сессию полученные данные о пользователе.
            //Они понадобятся для регистрации, если пользователь не авторизован
            $this->setSessionByDescription($descriptionSocialResponse);

            /**
             * Пытаемся авторизовать пользователя с помощью соцсети
             */
            $authObj = $this->crmClient->request(new AuthorizeParticipantMethodDescription([
                'channel' => ParamsDefinition::SOCIAL_CHANNEL,
                'login' => $descriptionSocialResponse->getFormatSocialId(),
            ]));

            /** @var DescriptionAuthorizeParticipantResponse() $responseDescription */
            $responseDescription = new DescriptionAuthorizeParticipantResponse($authObj);


            /**
             * Если статус положителен авторизуем пользователя,
             * если нет - пробуем зарегистрировать
             */
            if ($responseDescription->getStatus() === true) {

                $user = $this->loginUser($authObj);

            }else if($responseDescription->getError() == DescriptionAuthorizeParticipantResponse::ERROR_PARTICIPANT_NOT_FOUND){
                $ilp = $this->container->get('adw_ilp.service');

                try{
                    $registerObj = $ilp->request(new RegisterParticipantMethodDescription($descriptionSocialResponse->getFieldsToArrayForRegistration()));
                }catch (\Exception $e){
                    return $user;
                }

                /** @var DescriptionRegistrationResponse $responseDescription */
                $responseDescription = new DescriptionRegistrationResponse($registerObj);

                /*
                 * Если регистраци успешна попробуем авторизовать пользователя
                 * */
                if($responseDescription->getStatus() === true){
                    $user = $this->loginUser($authObj);
                }
            }
        }

        return $user;
    }


    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$this->supportsClass(get_class($user))) {
            throw new UnsupportedUserException(sprintf('Unsupported user class "%s"', get_class($user)));
        }

        return $user;
    }


    /**
     * @param BaseDescriptionSocialResponse $descriptionSocialResponse
     * @return bool
     */
    protected function setSessionByDescription(BaseDescriptionSocialResponse $descriptionSocialResponse)
    {
        $session = new Session();

        $session->remove('regData');
        $session->set('regData', [
            $descriptionSocialResponse->getFieldsToArray()
        ]);

        return true;
    }

    /**
     * Загрузка данных о пользователе из ЦРМ по токену +
     * Получение из этих данных пользователя +
     * Установка пользователя в текущий токен
     * @param ILPResponseBody $authObj
     * @return Participant
     */
    protected function loginUser($authObj){

        /**
         * В $user лежит объект с пользовательскими данными.
         * @var Participant $user
         */
        $user = $this->loadUserByTokenAndSaveTokenToLocal($authObj->getToken());

        $authenticatedToken = $this->setTokenByUser($authObj, $user);

        if($authenticatedToken instanceof IlpAuthUserToken){
            $this->authenticatedToken = $authenticatedToken;
            $this->tokenStorage->setToken($authenticatedToken);
        }

        /**
         * Вернём объект с пользовательскими данными.
         */
        return $user;
    }

    /**
     * @return IlpAuthUserToken
     */
    public function getAuthenticatedToken()
    {
        return $this->authenticatedToken;
    }

    /**
     * @param IlpAuthUserToken $authenticatedToken
     * @return $this
     */
    public function setAuthenticatedToken($authenticatedToken)
    {
        $this->authenticatedToken = $authenticatedToken;
        return $this;
    }

}