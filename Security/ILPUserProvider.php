<?php

namespace ADW\IlpCrmBundle\Security;

use ADW\IlpCrmBundle\Entity\Participant;
use ADW\IlpCrmBundle\Method\Customer\AuthorizeParticipantMethodDescription;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use ADW\IlpCrmBundle\Service\RestClientService;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class ILPUserProvider
 * @package CrmBundle\Security
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ILPUserProvider implements UserProviderInterface
{
    use UserProviderTrait;

    /**
     * @var Registry
     */
    protected $doctrine;
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var RestClientService
     */
    protected $crmClient;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * ILPUserProvider constructor.
     * @param Registry $doctrine
     * @param TokenStorageInterface $tokenStorage
     * @param RestClientService $crmClient
     * @param ContainerInterface $container
     */
    public function __construct(Registry $doctrine, TokenStorageInterface $tokenStorage, $crmClient, $container) {
        $this->doctrine = $doctrine;
        $this->tokenStorage = $tokenStorage;
        $this->crmClient = $crmClient;
        $this->container = $container;
    }


    /**
     * @param string $username
     * @throws \Exception
     */
    public function loadUserByUsername($username)
    {
        throw new \Exception('You should use "loadUserByToken" method');
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function refreshUser(UserInterface $user)
    {
        return $user;
    }

    /**
     * @param $class
     * @return bool
     */
    public function supportsClass($class)
    {
        return $class === Participant::class;
    }

    /**
     * Получение пользовательского токена из црм по логину и паролю
     * @param IlpAuthUserToken $token
     * @return IlpAuthUserToken|bool
     */
    public function authorizeInCrm(IlpAuthUserToken $token)
    {
        $authObj = $this->crmClient->request(
            new AuthorizeParticipantMethodDescription(
                [
                    'channel' => ParamsDefinition::DEFAULT_CHANNEL,
                    'login' => $token->username,
                    'password' => $token->password,
                ]
            )
        );

        /**
         * В $user лежит объект с пользовательскими данными.
         * @var Participant $user
         */
        $user = $this->loadUserByTokenAndSaveTokenToLocal($authObj->getToken());

        $authenticatedToken = $this->setTokenByUser($authObj, $user);

        if($authenticatedToken instanceof IlpAuthUserToken){
            $token = $authenticatedToken;
        }

        return $token;
    }
}