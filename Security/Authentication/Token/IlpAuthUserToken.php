<?php

namespace ADW\IlpCrmBundle\Security\Authentication\Token;

use Symfony\Component\Security\Core\Authentication\Token\AbstractToken;

/**
 * Class IlpAuthUserToken
 * @package ADW\IlpCrmBundle\Security\Authentication\Token
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class IlpAuthUserToken extends AbstractToken
{
    public $username;
    public $password;

    /**
     * IlpAuthUserToken constructor.
     * @param array $roles
     */
    public function __construct(array $roles = array())
    {
        parent::__construct($roles);

        $this->setAuthenticated(count($roles) > 0);
    }

    /**
     * @return string
     */
    public function getCredentials()
    {
        return '';
    }
}