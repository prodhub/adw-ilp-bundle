<?php

/*
 * This file is part of the HWIOAuthBundle package.
 *
 * (c) Hardware.Info <opensource@hardware.info>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ADW\IlpCrmBundle\Security\Authentication\Provider;

use ADW\IlpCrmBundle\Entity\Participant;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use ADW\IlpCrmBundle\Security\SocialUserProvider;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\OAuthAwareExceptionInterface;
use HWI\Bundle\OAuthBundle\Security\Core\User\OAuthAwareUserProviderInterface;
use HWI\Bundle\OAuthBundle\Security\Http\ResourceOwnerMap;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationServiceException;

/**
 * OAuthProvider.
 */
class IlpSocialOAuthProvider implements AuthenticationProviderInterface
{
    /**
     * @var ResourceOwnerMap
     */
    private $resourceOwnerMap;

    /**
     * @var OAuthAwareUserProviderInterface
     */
    private $userProvider;

    /**
     * @var UserCheckerInterface
     */
    private $userChecker;

    /**
     * @param OAuthAwareUserProviderInterface $userProvider     User provider
     * @param ResourceOwnerMap                $resourceOwnerMap Resource owner map
     * @param UserCheckerInterface            $userChecker      User checker
     */
    public function __construct(OAuthAwareUserProviderInterface $userProvider, ResourceOwnerMap $resourceOwnerMap, UserCheckerInterface $userChecker)
    {
        $this->userProvider = $userProvider;
        $this->resourceOwnerMap = $resourceOwnerMap;
        $this->userChecker = $userChecker;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(TokenInterface $token)
    {
        return
            $token instanceof OAuthToken
            && $this->resourceOwnerMap->hasResourceOwnerByName($token->getResourceOwnerName())
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function authenticate(TokenInterface $token)
    {
        if (!$this->supports($token)) {
            return;
        }

        /** @var SocialUserProvider $userProvider */
        $userProvider = $this->userProvider;

        /* @var OAuthToken $token */
        $resourceOwner = $this->resourceOwnerMap->getResourceOwnerByName($token->getResourceOwnerName());

        $userResponse = $resourceOwner->getUserInformation($token->getRawToken());

        try {
            /** @var Participant $user */
            $user = $userProvider->loadUserByOAuthUserResponse($userResponse);
            $ilpToken = $userProvider->getAuthenticatedToken();
        } catch (OAuthAwareExceptionInterface $e) {
            $e->setToken($token);
            $e->setResourceOwnerName($token->getResourceOwnerName());

            throw $e;
        }

        if (!$user instanceof UserInterface) {
            throw new AuthenticationServiceException('loadUserByOAuthUserResponse() must return a UserInterface.');
        }

        /**
         * Расширен стандартный authenticate() HWIBundle
         * Добавлена возможность установки кастомного
         * AuthUserToken инициализированного в ADW\IlpCrmBundle\Security\SocialUserProvider
         * */
        if($ilpToken instanceof IlpAuthUserToken){
            $token = $ilpToken;
        }else{
            $this->userChecker->checkPreAuth($user);
            $this->userChecker->checkPostAuth($user);

            $token = new OAuthToken($token->getRawToken(), $user->getRoles());
            $token->setResourceOwnerName($resourceOwner->getName());
            $token->setUser($user);
            $token->setAuthenticated(true);
        }

        return $token;
    }
}
