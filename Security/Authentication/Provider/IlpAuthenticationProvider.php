<?php

namespace ADW\IlpCrmBundle\Security\Authentication\Provider;

use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use ADW\IlpCrmBundle\Security\ILPUserProvider;
use Symfony\Component\Security\Core\Authentication\Provider\AuthenticationProviderInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * Class IlpAuthenticationProvider
 * @package ADW\IlpCrmBundle\Security\Authentication\Provider
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class IlpAuthenticationProvider implements AuthenticationProviderInterface
{
    /**
     * @var ILPUserProvider
     */
    private $userProvider;
    /**
     * @var
     */
    private $crmClient;

    /**
     * IlpAuthenticationProvider constructor.
     * @param ILPUserProvider $userProvider
     * @param $crmClient
     */
    public function __construct(ILPUserProvider $userProvider, $crmClient)
    {
        $this->userProvider = $userProvider;
        $this->crmClient = $crmClient;
    }

    /**
     * @param TokenInterface $token
     * @return IlpAuthUserToken|TokenInterface
     */
    public function authenticate(TokenInterface $token)
    {
        return $this->userProvider->authorizeInCrm($token);
    }

    /**
     * @param TokenInterface $token
     * @return bool
     */
    public function supports(TokenInterface $token)
    {
        return $token instanceof IlpAuthUserToken;
    }
}