<?php

namespace ADW\IlpCrmBundle\Security\Firewall;

use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Http\Firewall\ListenerInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use ADW\IlpCrmBundle\ParamsDefinition;

/**
 * Class IlpAuthenticationListener
 * @package ADW\IlpCrmBundle\Security\Firewall
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class IlpAuthenticationListener implements ListenerInterface
{
    protected $tokenStorage;

    protected $authenticationManager;

    /**
     * IlpAuthenticationListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param AuthenticationManagerInterface $authenticationManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authenticationManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authenticationManager = $authenticationManager;
    }

    /**
     * @param GetResponseEvent $event
     */
    public function handle(GetResponseEvent $event)
    {

        if ($event->getRequest()->get('_route') == ParamsDefinition::AUTH_ROUTE) {
            $this->login($event);
        }

        if ($event->getRequest()->get('_route') == ParamsDefinition::EXIT_ROUTE) {
            $this->logout();
        }
    }

    /**
     * @param GetResponseEvent $event
     */
    protected function login(GetResponseEvent $event)
    {
        $request = $event->getRequest();

        $login = $request->get('login');
        $password = $request->get('password');

        $token = new IlpAuthUserToken();
        $token->username = $login;
        $token->password = $password;
        
        $authToken = $this->authenticationManager->authenticate($token);

        if ($authToken) {
            $this->tokenStorage->setToken($authToken);
        }
    }


    protected function logout()
    {
        $this->tokenStorage->setToken(null);
    }
}