<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 02/10/2017
 * Time: 12:36
 */

namespace ADW\IlpCrmBundle\Security;

use ADW\IlpCrmBundle\Entity\Participant;
use ADW\IlpCrmBundle\Entity\Token;
use ADW\IlpCrmBundle\Method\Customer\GetParticipantDataByTokenMethodDescription;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;

trait UserProviderTrait
{

    /**
     * @param $authObj
     * @param $user
     * @return IlpAuthUserToken|null
     */
    protected function setTokenByUser($authObj, $user)
    {
        $authenticatedToken = null;

        if ($user instanceof Participant) {
            $authenticatedToken = new IlpAuthUserToken($user->getRoles());
            $authenticatedToken->setUser($user);
            $authenticatedToken->setAttribute('crm_token', $authObj->getToken());
        }

        return $authenticatedToken;
    }

    /**
     * Загрузка данных о пользователе из ЦРМ по токену +
     * Получение из этих данных пользователя +
     * Сохранение пары токен+id participant в БД
     * @param string $token
     * @return mixed|Participant
     */
    public function loadUserByTokenAndSaveTokenToLocal($token)
    {
        $participant = $this->loadUserByToken($token);

        if (!($participant instanceof Participant)) {
            return false;
        }

        $this->saveTokenToLocal($participant->getId(), $token);

        return $participant;
    }

    /**
     * @param string $token
     * @return Participant|null
     */
    public function loadUserByToken($token)
    {
        $data = $this->crmClient->request(
            new GetParticipantDataByTokenMethodDescription(
                [
                    'token' => $token
                ]
            )
        );

        $participant = $data->getParticipant();

        return $participant;
    }

    /**
     * @param $id
     * @param $token
     */
    protected function saveTokenToLocal($id, $token)
    {
        $em = $this->doctrine->getManager();

        $tokenStorage = new Token();
        $tokenStorage->setUid($id);
        $tokenStorage->setToken($token);
        $em->persist($tokenStorage);
        $em->flush();
    }



}