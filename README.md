# ADWIlpCrmBundle

## Installation

В composer.json:

```
#composer.json
"repositories": [
    ...
   { 
        "type": "vcs", 
        "url": "https://bitbucket.org/prodhub/adw-ilp-bundle.git" 
   },
   {
       "type": "git",
       "url": "https://bitbucket.org/prodhub/rest-client-bundle.git"
   },
   {
       "type": "git",
       "url": "https://bitbucket.org/prodhub/guzzle-bundle.git"
   }
]
```


```
# AppKernel.php
$bundles = array(
	...
	new Nelmio\ApiDocBundle\NelmioApiDocBundle(),
    new JMS\SerializerBundle\JMSSerializerBundle(),
    new ADW\IlpCrmBundle\ADWIlpCrmBundle,
)
```

## Security options
```
#app/config/security.yml

...
security:

    providers:
        adw_crm_user:
            id: adw_ilp.user_provider


    firewalls:
        dev:
            pattern: ^/(_(profiler|wdt)|css|images|js)/
            security: false

```

## Configuration (optional)
```
#app/config/parameters.yml

...
    ilp_secret_code: ~
    ilp_username: ~
    ilp_pass: ~
    ilp_api_url: ~

```

```
#app/config/routing.yml

...

adw_ilp:
    resource: '@ADWIlpCrmBundle/Resources/config/routing.yml'

```

Добавить настройки для API-DOC ( https://symfony.com/doc/current/bundles/NelmioApiDocBundle/index.html )

JMSSerializerBundle и NelmioApiDocBundle - подключать, только, если они не были подключены ранее

## Настройки конфигураций для регистрации и авторизации через соц Сети (в примере ФБ и ВК)
```
#app/config/security.yml

...
security:

   ...
   
    firewalls:
        
         ...
         
         main:
             anonymous: ~
             oauth:
                  resource_owners:
                      facebook:           "/social/login/check-facebook"
                      vkontakte:          "/social/login/check-vkontakte"
                  login_path:        /social/login
                  use_forward:       false
                  failure_path:      /social/login
                  oauth_user_provider:
                      service: adw_ilp_user.oauth_user_provider
             pattern:                ^/
             provider:               adw_crm_user
             adw_ilp:                true
        
        
    access_control:
        - { path: ^/social/login/check-facebook, roles: IS_AUTHENTICATED_ANONYMOUSLY }
        - { path: ^/social/login/check-vkontakte, roles: IS_AUTHENTICATED_ANONYMOUSLY }
```

```
#app/config/config.yml

...
 
hwi_oauth:
    firewall_names: [main]
    resource_owners:
        vkontakte:
            type:                vkontakte
            client_id:           '%vk_client_id%'
            client_secret:       '%vk_client_secret%'
            scope:               email,status
            options:
                fields:          uid,first_name,last_name,screen_name,sex,bdate
        facebook:
            type:                facebook
            client_id:           "%facebook_app_id%"
            client_secret:       "%facebook_app_secret%"
            options:
                  display: popup #dialog is optimized for popup window
                  auth_type: rerequest # Re-asking for Declined Permissions
            scope:               "email, basic_info, user_birthday"
            infos_url:           "https://graph.facebook.com/me?fields=id,first_name,last_name,name,gender,birthday,email"
            response_type: code
            paths:
                email: email
                firstname: first_name
                lastname: last_name
                gender: gender
                birthday: birthday
                profilepicture: picture.data.url
                locale: locale

```

```
#app/config/parameters.yml

...
    facebook_app_id: ~
    facebook_app_secret: ~
    vk_client_id: ~
    vk_client_secret: ~

```

В результате авторизация(регитстрация) через соц. сети доступна по адресу /social/login/
После успешной авторизации(регитстрации) редирект на главную
Более подробно https://github.com/hwi/HWIOAuthBundle


## Перед запуском

Если в проекте используются миграции, то создать её и накатить

```
php app/console doctrine:migrations:diff
php app/console doctrine:migrations:migrate

```
Если нет, то обновите схему БД
```
php app/console doctrine:schema:update --force

```