<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionPasswordRecoveryResponse extends BaseDescriptionResponse
{
    const ERROR_PARTICIPANT_NOT_FOUND = 'Участник не найден';
    const ERROR_PROCESSING_REQUEST = 'Ошибка обработки запроса';

    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Успешно, пароль изменен:
                 */
                case '1':
                    $status = true;
                    break;

                /**
                 * Ошибка, участник не найден:
                 */
                case '2':
                    $this->error = self::ERROR_PARTICIPANT_NOT_FOUND;
                    break;

                /**
                 * Ошибка обработки запроса:
                 */
                case '3':
                    $this->error = self::ERROR_PROCESSING_REQUEST;
                    break;


                /**
                 * Неизвестный ответ:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}