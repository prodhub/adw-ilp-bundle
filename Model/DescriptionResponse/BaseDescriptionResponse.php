<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:03
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


use ADW\IlpCrmBundle\Model\Response\ILPResponseBody;

/**
 * Class BaseDescriptionResponse
 * @package ADW\IlpCrmBundle\Model\DescriptionResponse
 */
abstract class BaseDescriptionResponse
{
    const ERROR_INCORRECT_RESPONSE = 'Некорректный ответ CRM';

    const ERROR_UNKNOWN_ERROR = "Неизвестная ошибка";

    const ERROR_UNKNOWN_ANSWER = "Неизвестный ответ";


    /**
     * @var ILPResponseBody
     */
    protected $ilpResponse;

    /**
     * @var string
     */
    protected $error;


    /**
     * BaseDescriptionResponse constructor.
     * @param ILPResponseBody $ilpResponse
     */
    public function __construct(ILPResponseBody $ilpResponse)
    {
        $this->ilpResponse = $ilpResponse;
    }

    /**
     * @return bool
     */
    abstract public function getStatus();


    /**
     * @return string
     */
    public function getError(){

        return $this->error;
    }

    /**
     * @return mixed
     */
    public function getLimitations()
    {
        return $this->ilpResponse->getLimitations();
    }

    /**
     * @return mixed
     */
    public function getBadFields()
    {
        return $this->ilpResponse->getBadFields();
    }

    /**
     * @return mixed
     */
    public function getWarningFields()
    {
        return $this->ilpResponse->getWarningFields();
    }
}