<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionRegistrationResponse extends BaseDescriptionResponse
{
    const ERROR_REGISTRATION = 'Участник не зарегистрирован';
    const ERROR_REGISTRATION_USER_EXISTS = 'Пользователь с такими данными уже зарегистрирован в системе';

    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Успешная регистрация:
                 */
                case '1':
                    $status = true;
                    break;

                /**
                 * Участник не зарегистрирован :
                 */
                case '2':
                    $this->error = self::ERROR_REGISTRATION;
                    break;

                /**
                 * Участник с таким уникальным полем уже зарегистрирован :
                 */
                case '3':
                    $this->error = self::ERROR_REGISTRATION_USER_EXISTS;
                    break;

                /**
                 * Неизвестный ответ регистрации:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}