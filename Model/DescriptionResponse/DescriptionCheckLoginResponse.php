<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionCheckLoginResponse extends BaseDescriptionResponse
{
    const ERROR_LOGIN_IS_NOT_UNIQUE = 'Логин не уникален';

    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Логин уникален:
                 */
                case '1':
                    $status = true;
                    break;

                /**
                 * Логин не уникален:
                 */
                case '2':
                    $this->error = self::ERROR_LOGIN_IS_NOT_UNIQUE;
                    break;


                /**
                 * Неизвестный ответ:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}