<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionUpdateParticipantResponse extends BaseDescriptionResponse
{
    const ERROR_UPDATE = 'Ошибка обновления данных участника (неверный формат поля  / не соответствует справочнику)';
    const ERROR_REGISTRATION_USER_EXISTS = 'Пользователь с такими данными уже зарегистрирован в системе';

    protected $badFields;

    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Успешная обновление:
                 */
                case '1':
                    $status = true;
                    break;

                /**
                 * Ошибка обновления данных :
                 */
                case '2':
                    $this->error = self::ERROR_UPDATE;
                    break;

                /**
                 * Участник с таким уникальным полем уже зарегистрирован :
                 */
                case '3':
                    $this->error = self::ERROR_REGISTRATION_USER_EXISTS;
                    break;

                /**
                 * Неизвестный ответ регистрации:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}