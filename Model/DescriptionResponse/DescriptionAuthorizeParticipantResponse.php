<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionAuthorizeParticipantResponse extends BaseDescriptionResponse
{
    const ERROR_PARTICIPANT_NOT_FOUND = 'Участник не найден';
    const ERROR_INCORRECT_PASSWORD = 'Неверный пароль';
    const ACTIVE_PARTICIPANT_STATUS = '1';
    const NOT_ACTIVE_PARTICIPANT_STATUS = '2';


    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Успешно, выдан токен неактивированного участника
                 */
                case self::ACTIVE_PARTICIPANT_STATUS:
                    $status = true;
                    break;

                /**
                 * Успешно, выдан токен активированного участника
                 */
                case self::NOT_ACTIVE_PARTICIPANT_STATUS:
                    $status = true;
                    break;

                /**
                 * Ошибка, участник не найден
                 */
                case '3':
                    $this->error = self::ERROR_PARTICIPANT_NOT_FOUND;
                    break;

                /**
                 * Ошибка, неверный пароль
                 */
                case '4':
                    $this->error = self::ERROR_INCORRECT_PASSWORD;
                    break;

                /**
                 * Неизвестная ошибка
                 */
                case '5':
                    $this->error = self::ERROR_UNKNOWN_ERROR;
                    break;


                /**
                 * Неизвестный ответ:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}