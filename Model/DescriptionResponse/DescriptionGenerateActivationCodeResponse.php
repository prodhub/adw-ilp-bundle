<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 12:07
 */

namespace ADW\IlpCrmBundle\Model\DescriptionResponse;


class DescriptionGenerateActivationCodeResponse extends BaseDescriptionResponse
{
    const ERROR_PHONE_ALREADY_ACTIVE = 'Мобильный тедефон уже активирован';
    const ERROR_EMAIL_ALREADY_ACTIVE = 'Email уже активирован';

    public function getStatus()
    {
        $status = false;

        if (method_exists($this->ilpResponse, 'getResult')) {
            switch ($this->ilpResponse->getResult()) {

                /**
                 * Успешно код для телефона создан
                 */
                case '1':
                    $status = true;
                    break;

                /**
                 * Успешно код для email создан
                 */
                case '2':
                    $status = true;
                    break;

                /**
                 * Error. Мобильный тедефон уже активирован
                 */
                case '3':
                    $this->error = self::ERROR_PHONE_ALREADY_ACTIVE;
                    break;

                /**
                 * Error. Email уже активирован
                 */
                case '4':
                    $this->error = self::ERROR_EMAIL_ALREADY_ACTIVE;
                    break;

                /**
                 * Неизвестный ответ:
                 */
                default:
                    $this->error = self::ERROR_UNKNOWN_ANSWER;
                    break;
            }
        }else{
            $this->error = self::ERROR_INCORRECT_RESPONSE;
        }

        return $status;
    }

}