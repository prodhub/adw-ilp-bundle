<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class LimitationModel
 * @package ADW\IlpCrmBundle\Model\Response
 */
class LimitationModel
{

    /**
     * ID ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $id;

    /**
     * Тип ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $type;

    /**
     * Название ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $name;

    /**
     * Описание ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $description;

    /**
     * Дата и время начала срока действия ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $start_date;

    /**
     * Дата и время завершения срока действия ограничения
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $end_date;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->start_date;
    }

    /**
     * @param string $start_date
     * @return $this
     */
    public function setStartDate($start_date)
    {
        $this->start_date = $start_date;
        return $this;
    }

    /**
     * @return string
     */
    public function getEndDate()
    {
        return $this->end_date;
    }

    /**
     * @param string $end_date
     * @return $this
     */
    public function setEndDate($end_date)
    {
        $this->end_date = $end_date;
        return $this;
    }



}