<?php

namespace ADW\IlpCrmBundle\Model\Response;

use ADW\IlpCrmBundle\Entity\Participant;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class AuthorizedParticipantModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class AuthorizedParticipantModel extends ILPResponseBody
{
    /**
     * @Serialized\Type("ADW\IlpCrmBundle\Entity\Participant")
     * @var Participant
     */
    public $participant;

    /**
     * @return mixed
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param mixed $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }
}