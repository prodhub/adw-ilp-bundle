<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class RegisterParticipantModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class RegisterParticipantModel extends ILPResponseBody
{

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $id;

    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $token;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}