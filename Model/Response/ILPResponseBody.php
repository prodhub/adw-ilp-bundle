<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ILPResponseBody
 * @package ADW\IlpCrmBundle\Model\Response
 * @Serialized\XmlRoot("response")
 */
class ILPResponseBody
{
    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */

    protected $error;

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $result;

    /**
     * @var integer
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $resultmsg;

    /**
     * @var array
     *
     * @Serialized\Type("ADW\IlpCrmBundle\Model\Response\WarningFieldModel")
     * @Serialized\SerializedName("warning_fields")
     */
    protected $warning_fields;

    /**
     * @var array
     *
     * @Serialized\Type("ADW\IlpCrmBundle\Model\Response\BadFieldModel")
     * @Serialized\SerializedName("bad_fields")
     */
    protected $bad_fields;

    /**
     * Узел с данными обо всех действующих ограничениях
     * @var array
     *
     * @Serialized\Type("ADW\IlpCrmBundle\Model\Response\LimitationModel")
     * @Serialized\SerializedName("limitations")
     */
    protected $limitations;

    /**
     * @return int
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param int $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return int
     */
    public function getResultmsg()
    {
        return $this->resultmsg;
    }

    /**
     * @param int $resultmsg
     */
    public function setResultmsg($resultmsg)
    {
        $this->resultmsg = $resultmsg;
    }

    /**
     * @return array
     */
    public function getWarningFields()
    {
        return $this->warning_fields;
    }

    /**
     * @param array $warning_fields
     * @return $this
     */
    public function setWarningFields($warning_fields)
    {
        $this->warning_fields = $warning_fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getBadFields()
    {
        return $this->bad_fields;
    }

    /**
     * @param array $bad_fields
     * @return $this
     */
    public function setBadFields($bad_fields)
    {
        $this->bad_fields = $bad_fields;
        return $this;
    }

    /**
     * @return array
     */
    public function getLimitations()
    {
        return $this->limitations;
    }

    /**
     * @param array $limitations
     * @return $this
     */
    public function setLimitations($limitations)
    {
        $this->limitations = $limitations;
        return $this;
    }


}