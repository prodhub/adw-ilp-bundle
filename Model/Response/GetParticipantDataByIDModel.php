<?php

namespace ADW\IlpCrmBundle\Model\Response;

use ADW\IlpCrmBundle\Entity\Participant;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class GetParticipantDataByIDModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetParticipantDataByIDModel extends ILPResponseBody
{
    /**
     * @var Participant
     * @Serialized\Type("ADW\IlpCrmBundle\Entity\Participant")
     * @Serialized\XmlList(entry="participant")
     */
    public $participant;

    /**
     * @return Participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param Participant $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }
}