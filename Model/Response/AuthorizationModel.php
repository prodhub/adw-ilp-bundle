<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class AuthorizationModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class AuthorizationModel extends ILPResponseBody
{
    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $token;

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }
}