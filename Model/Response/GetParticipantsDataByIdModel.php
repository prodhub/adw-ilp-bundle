<?php

namespace ADW\IlpCrmBundle\Model\Response;

use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class GetParticipantsDataByIdModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class GetParticipantsDataByIdModel extends ILPResponseBody
{
    /**
     * @var array
     * @Serialized\Type("ArrayCollection<ADW\IlpCrmBundle\Model\Response\ParticipantsModel>")
     * @Serialized\XmlList(entry="participant")
     */
    public $participants;

    /**
     * GetParticipantsDataByIdModel constructor.
     */
    public function __construct()
    {
        $this->participants = new ArrayCollection();
    }

    /**
     * @return array
     */
    public function getParticipants()
    {
        return $this->participants;
    }

    /**
     * @param array $participants
     */
    public function setParticipants($participants)
    {
        $this->participants = $participants;
    }


}