<?php

namespace ADW\IlpCrmBundle\Model\Response;

use ADW\IlpCrmBundle\Entity\Participant;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class CheckLoginUniquenessModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class CheckLoginUniquenessModel extends ILPResponseBody
{
    /**
     * @var Participant
     * @Serialized\Type("ADW\IlpCrmBundle\Entity\Participant")
     */
    protected $participant;

    /**
     * @return Participant
     */
    public function getParticipant()
    {
        return $this->participant;
    }

    /**
     * @param mixed $participant
     */
    public function setParticipant($participant)
    {
        $this->participant = $participant;
    }
}