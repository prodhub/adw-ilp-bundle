<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class BadFieldModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class BadFieldModel
{
    /**
     * @var string
     * @Serialized\Type("string")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $birthdate;

    /**
     * @return string
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param string $birthdate
     * @return $this
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;
        return $this;
    }


}