<?php

namespace ADW\IlpCrmBundle\Model\Response;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ParticipantsModel
 * @package ADW\IlpCrmBundle\Model\Response
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ParticipantsModel
{

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $id;

    /**
     * @var integer
     * @Serialized\Type("integer")
     * @Serialized\XmlElement(cdata=false)
     */
    protected $result;

    /**
     * @Serialized\Type("ADW\IlpCrmBundle\Entity\Participant")
     */
    public $profile_data;

    /**
     * @return mixed
     */
    public function getProfileData()
    {
        return $this->profile_data;
    }

    /**
     * @param mixed $profile_data
     */
    public function setProfileData($profile_data)
    {
        $this->profile_data = $profile_data;
    }

    /**
     * @return int
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param int $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}