<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28/09/2017
 * Time: 14:43
 */

namespace ADW\IlpCrmBundle\Model\DescriptionSocialResponse;



class VkDescriptionSocialResponse extends BaseDescriptionSocialResponse
{
    const SOCIAL_NAME = 'vkontakte';

    public function getFormatSocialId()
    {
        return sprintf('%s %d', 'vk', $this->response->getResponse()['response'][0]['uid']);
    }

    public function getRegisterSocialFieldName()
    {
        return 'vk_id';
    }

    public function getFirstnameFromResponse()
    {
        return $this->response->getFirstName();
    }

    public function getLastnameFromResponse()
    {
        return $this->response->getLastName();
    }

    public function getBirthdateFromResponse()
    {
        return isset($this->response->getResponse()['response'][0]['bdate']) ? date('d.m.Y', strtotime($this->response->getResponse()['response'][0]['bdate'])) : '';
    }

    public function getSexFromResponse()
    {
        return isset($this->response->getResponse()['response'][0]['bdate']) ? $this->response->getResponse()['response'][0]['sex'] == 2 ? 'Y' : 'N' : '';
    }

    public function getEmailFromResponse()
    {
        return $this->response->getEmail();
    }

    public function getSocialType()
    {
        return 'vk';
    }

    public function getSocialId()
    {
        return  $this->response->getResponse()['response'][0]['uid'];
    }


}