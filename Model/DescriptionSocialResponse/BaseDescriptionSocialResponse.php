<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28/09/2017
 * Time: 14:18
 */

namespace ADW\IlpCrmBundle\Model\DescriptionSocialResponse;

use \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;
use \ADW\IlpCrmBundle\ParamsDefinition;


abstract class BaseDescriptionSocialResponse
{

    /**
     * @var \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface
     */
    protected $response;

    /**
     * @var string
     */
    protected $firstname;


    /**
     * @var string
     */
    protected $lastname;


    /**
     * @var string
     */
    protected $birthdate;


    /**
     * @var string
     */
    protected $sex;


    /**
     * @var string
     */
    protected $email;


    /**
     * @var string
     */
    protected $socialType;


    /**
     * @var string
     */
    protected $socialId;

    /**
     * BaseDescriptionSocialResponse constructor.
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     */
    public function __construct(UserResponseInterface $response)
    {
        $this->response = $response;
    }

    public function getFieldsToArray()
    {
        return [
            'registerType' => ParamsDefinition::SOCIAL_CHANNEL,
            'firstname' => $this->getFirstnameFromResponse(),
            'lastname' => $this->getLastnameFromResponse(),
            'birthdate' => $this->getBirthdateFromResponse(),
            'sex' => $this->getSexFromResponse(),
            'email' => $this->getEmailFromResponse(),
            'socialType' => $this->getSocialType(),
            'socialId' => $this->getSocialId(),
        ];
    }

    public function getFieldsToArrayForRegistration()
    {
        return [
            'channel' => ParamsDefinition::SOCIAL_CHANNEL,
            'firstname' => $this->getFirstnameFromResponse(),
            'lastname' => $this->getLastnameFromResponse(),
            'birthdate' => !empty($this->getBirthdateFromResponse()) ? $this->getBirthdateFromResponse() : "01.01.2000",
            'ismale' => $this->getSexFromResponse(),
            'email' => $this->getEmailFromResponse(),
            'isrulesagreed' => 'Y',
            'ispdagreed' => 'Y',
            'ismailingagreed' => 'Y',
            $this->getRegisterSocialFieldName() => $this->getFormatSocialId(),
            'password' => uniqid('ilp_')
        ];
    }

    /**
     * @return string
     */
    abstract public function getFormatSocialId();

    /**
     * @return string
     */
    abstract public function getRegisterSocialFieldName();

    /**
     * @return string
     */
    abstract public function getFirstnameFromResponse();

    /**
     * @return string
     */
    abstract public function getLastnameFromResponse();

    /**
     * @return string
     */
    abstract public function getBirthdateFromResponse();

    /**
     * @return string
     */
    abstract public function getSexFromResponse();

    /**
     * @return string
     */
    abstract public function getEmailFromResponse();

    /**
     * @return string
     */
    abstract public function getSocialType();

    /**
     * @return string
     */
    abstract public function getSocialId();

}