<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28/09/2017
 * Time: 14:30
 */

namespace ADW\IlpCrmBundle\Model\DescriptionSocialResponse;


use \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface;


class FactoryDescriptionSocialResponse
{
    /**
     * @var \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface
     */
    protected $response;

    /**
     * @var BaseDescriptionSocialResponse
     */
    protected $responseDescription;

    /**
     * BaseDescriptionSocialResponse constructor.
     * @param \HWI\Bundle\OAuthBundle\OAuth\Response\UserResponseInterface $response
     */
    public function __construct(UserResponseInterface $response)
    {
        $this->response = $response;

        switch ($response->getResourceOwner()->getName()){
            case FbDescriptionSocialResponse::SOCIAL_NAME:
                $this->responseDescription = new FbDescriptionSocialResponse($response);
                break;
            case VkDescriptionSocialResponse::SOCIAL_NAME:
                $this->responseDescription = new VkDescriptionSocialResponse($response);
                break;
            case OkDescriptionSocialResponse::SOCIAL_NAME:
                $this->responseDescription = new OkDescriptionSocialResponse($response);
                break;
            default:
                $this->responseDescription = null;
        }


    }

    /**
     * @return BaseDescriptionSocialResponse
     */
    public function getResponseDescription()
    {
        return $this->responseDescription;
    }

}