<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28/09/2017
 * Time: 14:43
 */

namespace ADW\IlpCrmBundle\Model\DescriptionSocialResponse;



class OkDescriptionSocialResponse extends BaseDescriptionSocialResponse
{
    const SOCIAL_NAME = 'odnoklassniki';

    public function getFormatSocialId()
    {
        return sprintf('%s %d', 'ok', $this->response->getResponse()['uid']);;
    }

    public function getRegisterSocialFieldName()
    {
        return 'ok_id';
    }

    public function getFirstnameFromResponse()
    {
        return $this->response->getFirstName();
    }

    public function getLastnameFromResponse()
    {
        return $this->response->getLastName();
    }

    public function getBirthdateFromResponse()
    {
        return date('d.m.Y', strtotime($this->response->getResponse()['birthday']));;
    }

    public function getSexFromResponse()
    {
        return $this->response->getResponse()['gender'] == 'male' ? 'Y' : 'N';;
    }

    public function getEmailFromResponse()
    {
        return $this->response->getEmail();
    }

    public function getSocialType()
    {
        return 'ok';
    }

    public function getSocialId()
    {
        return  $this->response->getResponse()['uid'];
    }


}