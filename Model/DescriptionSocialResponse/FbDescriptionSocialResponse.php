<?php
/**
 * Created by PhpStorm.
 * User: artem
 * Date: 28/09/2017
 * Time: 14:43
 */

namespace ADW\IlpCrmBundle\Model\DescriptionSocialResponse;



class FbDescriptionSocialResponse extends BaseDescriptionSocialResponse
{
    const SOCIAL_NAME = 'facebook';

    public function getFormatSocialId()
    {
        return sprintf('%s %d', 'fb', $this->response->getResponse()['id']);
    }

    public function getRegisterSocialFieldName()
    {
        return 'fb_id';
    }


    public function getFirstnameFromResponse()
    {
        return $this->response->getResponse()['first_name'];
    }

    public function getLastnameFromResponse()
    {
        return $this->response->getResponse()['last_name'];
    }

    public function getBirthdateFromResponse()
    {
        return isset($this->response->getResponse()['birthday']) ? date('d.m.Y', strtotime($this->response->getResponse()['birthday'])) : null;
    }

    public function getSexFromResponse()
    {
        return $this->response->getResponse()['gender'] == 'male' ? 'Y' : 'N';
    }

    public function getEmailFromResponse()
    {
        return isset($this->response->getResponse()['email'])?$this->response->getResponse()['email']:'';
    }

    public function getSocialType()
    {
        return 'fb';
    }

    public function getSocialId()
    {
        return $this->response->getResponse()['id'];
    }


}