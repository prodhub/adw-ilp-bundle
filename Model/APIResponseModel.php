<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 26/09/2017
 * Time: 15:31
 */

namespace ADW\IlpCrmBundle\Model;

use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Class APIResponseModel
 * @package ADW\IlpCrmBundle\Model
 */
class APIResponseModel
{
    /**
     * @var bool
     */
    protected $status;

    /**
     * @var string
     */
    protected $errorMessage;

    /**
     * @var array
     */
    protected $limitations;

    /**
     * @var array
     */
    protected $badFields;

    /**
     * @var array
     */
    protected $warningFields;

    /**
     * APIResponseModel constructor.
     */
    public function __construct()
    {
        $this->status = false;
        $this->errorMessage = '';
        $this->badFields = [];
        $this->warningFields = [];
        $this->limitations = [];
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return $this->errorMessage;
    }

    /**
     * @param string $errorMessage
     * @return $this
     */
    public function setErrorMessage($errorMessage)
    {
        $this->errorMessage = $errorMessage;
        return $this;
    }

    /**
     * @return array
     */
    public function getLimitations()
    {
        return $this->limitations;
    }

    /**
     * @param array $limitations
     * @return $this
     */
    public function setLimitations($limitations)
    {
        $this->limitations = $limitations;
        return $this;
    }


    /**
     * @return array
     */
    public function getBadFields()
    {
        return $this->badFields;
    }

    /**
     * @param array $badFields
     * @return $this
     */
    public function setBadFields($badFields)
    {
        $this->badFields = $badFields;
        return $this;
    }

    /**
     * @return array
     */
    public function getWarningFields()
    {
        return $this->warningFields;
    }

    /**
     * @param array $warningFields
     * @return $this
     */
    public function setWarningFields($warningFields)
    {
        $this->warningFields = $warningFields;
        return $this;
    }


    public function getResponseByJson()
    {
        $errors = [];

        if(!empty($this->errorMessage)){
            $errors['message'] = $this->errorMessage;
        }

        if(!empty($this->badFields)){
            $errors['bad_fields'] = $this->badFields;
        }

        if(!empty($this->warningFields)){
            $errors['warning_fields'] = $this->warningFields;
        }

        if(!empty($this->limitations)){
            $errors['limitations'] = $this->limitations;
        }

        return new JsonResponse(
        [
            'status' => $this->status,
            'errors' => $errors,
        ]);
    }

}