<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


class IsMaleField extends BaseField
{
    const NOT_VALID_FLAG = 'Flag is not valid (it should be Y/N)';

    static function getName()
    {
        return 'ismale';
    }

    /**
     * Extends standard valid check
     * @return bool
     */
    public function isValidateValue()
    {
        if(parent::isValidateValue()){
            if (!empty($this->value) && !($this->value == "Y" || $this->value == "N")) {
                $this->setError(self::NOT_VALID_FLAG);
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }
}