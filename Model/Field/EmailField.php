<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


class EmailField extends BaseField
{
    const NOT_VALID_EMAIL = 'Email is not valid';

    static function getName()
    {
        return 'email';
    }


    /**
     * Extends standard valid check
     * @return bool
     */
    public function isValidateValue()
    {
        //add check valid email
        if(parent::isValidateValue()){
            if (!filter_var($this->value, FILTER_VALIDATE_EMAIL)) {
                $this->setError(self::NOT_VALID_EMAIL);
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

}