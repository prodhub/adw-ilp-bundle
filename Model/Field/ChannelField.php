<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


use ADW\IlpCrmBundle\ParamsDefinition;

class ChannelField extends BaseField
{
    const NOT_VALID_CHANNEL = 'Channel is not valid';

    static function getName()
    {
        return 'channel';
    }

    /**
     * Extends standard valid check
     * @return bool
     */
    public function isValidateValue()
    {
        if(parent::isValidateValue()){
            if ($this->value == ParamsDefinition::DEFAULT_CHANNEL || $this->value == ParamsDefinition::SOCIAL_CHANNEL) {
                return true;
            }else{
                $this->setError(self::NOT_VALID_CHANNEL);
                return false;
            }
        }else{
            return false;
        }
    }
}