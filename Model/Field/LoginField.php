<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


class LoginField extends BaseField
{
    const NOT_VALID_LOGIN = 'Login is not valid';

    static function getName()
    {
        return 'login';
    }


    /**
     * Extends standard valid check
     * @return bool
     */
    public function isValidateValue()
    {
        //add check valid email
        if(parent::isValidateValue()){
            if (!$this->extendLoginValidate()) {
                $this->setError(self::NOT_VALID_LOGIN);
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

    /**
     * @return bool
     */
    protected function extendLoginValidate()
    {
        $result = false;

        /*
         * Если переданное занчение соответствует хоть одной из объявленных
         * в доке маскок
         * 7DEFXXXXXXX /  test@test.ts / vk 123  / ok 123 /  fb 123 /  gp 123
         * вернем true
         * */
        if (
            preg_match('/^7[0-9]{10}+$/',$this->value) ||
            filter_var($this->value, FILTER_VALIDATE_EMAIL) ||
            preg_match('/^vk [^ ]+|^ok [^ ]+|^fb [^ ]+|^gp [^ ]+/',$this->value)
        ){
            $result = true;
        }

        return $result;
    }

}