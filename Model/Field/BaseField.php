<?php

namespace ADW\IlpCrmBundle\Model\Field;


/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:43
 */

abstract class BaseField
{
    const EMPTY_ERROR_MESSAGE = 'Empty field';

    /**
     * @var string
     */
    protected $value;

    /**
     * @var bool
     */
    protected $require;

    /**
     * @var string
     */
    protected $error;


    /**
     * BaseField constructor.
     * @param string $value
     * @param bool $require
     */
    public function __construct($value, $require = false)
    {
        $this->setValue($value);
        $this->setRequire($require);
    }

    /**
     * @return string
     */
    abstract static function getName();


    /**
     * @return bool
     */
    public function isValidateValue()
    {
        if($this->isRequire() && empty($this->getValue())){
            $this->setError(self::EMPTY_ERROR_MESSAGE);
            return false;
        }else{
            return true;
        }
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return bool
     */
    public function isRequire()
    {
        return $this->require;
    }

    /**
     * @param bool $require
     * @return $this
     */
    public function setRequire($require)
    {
        $this->require = $require;
        return $this;
    }

    /**
     * @return string
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string $error
     * @return $this
     */
    public function setError($error)
    {
        $this->error = $error;
        return $this;
    }



}