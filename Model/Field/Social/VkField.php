<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field\Social;


use ADW\IlpCrmBundle\Model\Field\BaseField;

class VkField extends BaseField
{
    static function getName()
    {
        return 'vk_id';
    }

}