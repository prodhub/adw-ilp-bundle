<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


class DateField extends BaseField
{
    const NOT_VALID_DATE = 'Date is not valid';

    static function getName()
    {
        return 'birthdate';
    }

    /**
     * Extends standard valid check
     * @return bool
     */
    public function isValidateValue()
    {
        //add check valid email
        if(parent::isValidateValue()){
            $d = \DateTime::createFromFormat('d.m.Y', $this->value);
            if (!($d && $d->format('d.m.Y') === $this->value)) {
                $this->setError(self::NOT_VALID_DATE);
                return false;
            }else{
                return true;
            }
        }else{
            return false;
        }
    }

}