<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:49
 */

namespace ADW\IlpCrmBundle\Model\Field;


class TokenField extends BaseField
{
    static function getName()
    {
        return 'token';
    }

}