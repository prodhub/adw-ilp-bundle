<?php

namespace ADW\IlpCrmBundle\Model;

use ADW\IlpCrmBundle\Entity\Participant;
use JMS\Serializer\Annotation as Serialized;

/**
 * Class ProfileDataModel
 * @package ADW\IlpCrmBundle\Model
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ProfileDataModel
{
    /**
     * @Serialized\Type("ADW\IlpCrmBundle\Entity\Participant")
     * @var Participant
     */
    public $profile_data;

    /**
     * @return Participant
     */
    public function getProfileData()
    {
        return $this->profile_data;
    }

    /**
     * @param Participant $profile_data
     */
    public function setProfileData($profile_data)
    {
        $this->profile_data = $profile_data;
    }

}