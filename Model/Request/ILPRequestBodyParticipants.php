<?php

namespace ADW\IlpCrmBundle\Model\Request;

use JMS\Serializer\Annotation as Serialized;

/**
 * Class ILPRequestBodyParticipants
 * @package ADW\IlpCrmBundle\Model\Request
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ILPRequestBodyParticipants extends ILPRequestBody
{

    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("secretcode")
     */
    private $secretcode;
    /**
     * @var string
     *
     * @Serialized\Type("string")
     * @Serialized\SerializedName("action")
     */
    private $action;

    /**
     * @var array
     *
     * @Serialized\XmlKeyValuePairs()
     * @Serialized\SerializedName("params")
     * @Serialized\XmlList(entry="id")
     */
    protected $params;

    /**
     * ILPRequestBody constructor.
     * @param $action
     * @param $params
     */
    public function __construct($action, $params)
    {
        $this->action = $action;
        $this->params = $params;
    }

    /**
     * @return mixed
     */
    public function getSecretcode()
    {
        return $this->secretcode;
    }

    /**
     * @param mixed $secretcode
     * @return ILPRequestBody
     */
    public function setSecretcode($secretcode)
    {
        $this->secretcode = $secretcode;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param mixed $action
     * @return ILPRequestBody
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * @param array $params
     * @return ILPRequestBody
     */
    public function setParams($params)
    {
        $this->params = $params;
        return $this;
    }
}