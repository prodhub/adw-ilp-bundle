<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 19:07
 */
namespace ADW\IlpCrmBundle\Model\DescriptionRequest;

use ADW\IlpCrmBundle\Model\Field\ChannelField;
use ADW\IlpCrmBundle\Model\Field\LoginField;

use ADW\IlpCrmBundle\ParamsDefinition;
use \Symfony\Component\HttpFoundation\Request;


class DescriptionPasswordRecoveryRequest extends BaseDescriptionRequest
{

    protected function initFieldsFromRequest(Request $request)
    {
        $this->addField(new LoginField($request->get(LoginField::getName()), self::IS_REQUIRE));
    }

    protected function initAddedFields()
    {
        $this->addField(new ChannelField(ParamsDefinition::DEFAULT_CHANNEL, self::IS_REQUIRE));
    }

}