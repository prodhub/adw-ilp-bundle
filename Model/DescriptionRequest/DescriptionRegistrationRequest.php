<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 19:07
 */
namespace ADW\IlpCrmBundle\Model\DescriptionRequest;


use ADW\IlpCrmBundle\Model\Field\ChannelField;
use \ADW\IlpCrmBundle\Model\Field\CityField;
use \ADW\IlpCrmBundle\Model\Field\DateField;
use \ADW\IlpCrmBundle\Model\Field\EmailField;
use \ADW\IlpCrmBundle\Model\Field\FirstNameField;
use ADW\IlpCrmBundle\Model\Field\IsMailingAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsMaleField;
use ADW\IlpCrmBundle\Model\Field\IsPdAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsRulesAgreedField;
use \ADW\IlpCrmBundle\Model\Field\LastNameField;
use ADW\IlpCrmBundle\Model\Field\PasswordField;
use ADW\IlpCrmBundle\Model\Field\Social\FbField;
use ADW\IlpCrmBundle\Model\Field\Social\OkField;
use ADW\IlpCrmBundle\Model\Field\Social\VkField;


use \Symfony\Component\HttpFoundation\Request;


class DescriptionRegistrationRequest extends BaseDescriptionRequest
{

    protected function initFieldsFromRequest(Request $request)
    {
        $this->addField(new DateField($request->get(DateField::getName()), self::IS_REQUIRE))
            ->addField(new EmailField($request->get(EmailField::getName()), self::IS_REQUIRE))
            ->addField(new PasswordField($request->get(PasswordField::getName()), self::IS_REQUIRE))
            ->addField(new CityField($request->get(CityField::getName())))
            ->addField(new FirstNameField($request->get(FirstNameField::getName()), self::IS_REQUIRE))
            ->addField(new LastNameField($request->get(LastNameField::getName())))
            ->addField(new IsMaleField($request->get(IsMaleField::getName())))
            ->addField(new IsRulesAgreedField($request->get(IsRulesAgreedField::getName(), self::IS_REQUIRE)))
            ->addField(new IsPdAgreedField($request->get(IsPdAgreedField::getName(), self::IS_REQUIRE)))
            ->addField(new IsMailingAgreedField($request->get(IsMailingAgreedField::getName(), self::IS_REQUIRE)))
        ;

    }

    protected function initAddedFields()
    {
        $this->addField(new ChannelField($this->getRegistrationChannel()))
            ->addField(new VkField($this->getSocialIdFromSession('vk')))
            ->addField(new FbField($this->getSocialIdFromSession('fb')))
            ->addField(new OkField($this->getSocialIdFromSession('ok')));
    }

}