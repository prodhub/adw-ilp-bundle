<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 19:07
 */
namespace ADW\IlpCrmBundle\Model\DescriptionRequest;

use ADW\IlpCrmBundle\Model\Field\LoginField;

use \Symfony\Component\HttpFoundation\Request;


class DescriptionGenerateActivationCodeRequest extends BaseDescriptionRequest
{

    protected function initFieldsFromRequest(Request $request)
    {
        $this->addField(new LoginField($request->get(LoginField::getName()), self::IS_REQUIRE));

    }

    protected function initAddedFields()
    {

    }

}