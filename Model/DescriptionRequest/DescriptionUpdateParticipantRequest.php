<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 19:07
 */
namespace ADW\IlpCrmBundle\Model\DescriptionRequest;


use ADW\IlpCrmBundle\Model\Field\ChannelField;
use \ADW\IlpCrmBundle\Model\Field\CityField;
use \ADW\IlpCrmBundle\Model\Field\EmailField;
use \ADW\IlpCrmBundle\Model\Field\FirstNameField;
use ADW\IlpCrmBundle\Model\Field\IsMailingAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsMaleField;
use ADW\IlpCrmBundle\Model\Field\IsPdAgreedField;
use ADW\IlpCrmBundle\Model\Field\IsRulesAgreedField;
use \ADW\IlpCrmBundle\Model\Field\LastNameField;


use ADW\IlpCrmBundle\Model\Field\TokenField;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use \Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Session\Session;


class DescriptionUpdateParticipantRequest extends BaseDescriptionRequest
{

    /**
     * @var Session
     */
    protected $session;

    /**
     * @param OAuthToken|IlpAuthUserToken $token
     * @return $this
     */
    public function setToken($token)
    {
        $this->addField(new TokenField($token->getAttribute("crm_token"),true));
        return $this;
    }

    protected function initFieldsFromRequest(Request $request)
    {
        $this
            ->addFieldIfNotEmptyValue(new EmailField($request->get(EmailField::getName())))
            ->addFieldIfNotEmptyValue(new CityField($request->get(CityField::getName())))
            ->addFieldIfNotEmptyValue(new FirstNameField($request->get(FirstNameField::getName())))
            ->addFieldIfNotEmptyValue(new LastNameField($request->get(LastNameField::getName())))
            ->addFieldIfNotEmptyValue(new IsMaleField($request->get(IsMaleField::getName())))
            ->addFieldIfNotEmptyValue(new IsRulesAgreedField($request->get(IsRulesAgreedField::getName())))
            ->addFieldIfNotEmptyValue(new IsPdAgreedField($request->get(IsPdAgreedField::getName())))
            ->addFieldIfNotEmptyValue(new IsMailingAgreedField($request->get(IsMailingAgreedField::getName())))
        ;
    }

    protected function initAddedFields()
    {
        $this->session = new Session();

        $this->addField(new ChannelField($this->getRegistrationChannel()))
//            ->addField(new VkField($this->getSocialIdFromSession('vk')))
//            ->addField(new FbField($this->getSocialIdFromSession('fb')))
//            ->addField(new OkField($this->getSocialIdFromSession('ok')))
        ;
    }

}