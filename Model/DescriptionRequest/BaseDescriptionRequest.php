<?php
/**
 * Created by PhpStorm.
 * User: tzepart
 * Date: 25/09/2017
 * Time: 18:58
 */
namespace ADW\IlpCrmBundle\Model\DescriptionRequest;


use ADW\IlpCrmBundle\Model\Field\BaseField;
use \Doctrine\Common\Collections\ArrayCollection;
use \Symfony\Component\HttpFoundation\Request;

use Symfony\Component\HttpFoundation\Session\Session;


/**
 * Class BaseDescriptionRequest
 */
abstract class BaseDescriptionRequest
{
    const IS_REQUIRE = true;

    const SITE_REGISTRATION_CHANNEL = 'S';

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var \ADW\IlpCrmBundle\Model\Field\BaseField[]
     */
    protected $fields;

    /**
     * @var array
     */
    protected $errors;

    /**
     * BaseDescriptionRequest constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->fields = new ArrayCollection();

        //init session

        if($request->getSession() instanceof Session){
            $this->session = $request->getSession();
        }else{
            $this->session = new Session();
        }

        //init fields from request
        $this->initFieldsFromRequest($request);

        //init added fields
        $this->initAddedFields();
    }

    /**
     * @return array
     */
    public function getFieldsToArray()
    {
        $arFields = [];

        foreach ($this->fields as $index => $field) {
            $arFields[$field->getName()] = $field->getValue();
        }

        return $arFields;
    }

    /**
     * @return bool
     */
    public function isSuccessStatus()
    {
        $result = true;
        foreach ($this->fields as $index => $field) {
            //if one field is not valid, then return false
            if(!$field->isValidateValue()){
                $result = false;
            }
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        foreach ($this->fields as $index => $field) {
            if(!empty($field->getError())){
                $this->errors[$field->getName()] = $field->getError();
            }
        }

        return $this->errors;
    }

    /**
     * @param BaseField $field
     * @return $this
     */
    protected function addField(BaseField $field)
    {
        $this->fields->add($field);

        return $this;
    }

    /**
     * @param BaseField $field
     * @return $this
     */
    protected function addFieldIfNotEmptyValue(BaseField $field)
    {
        if(!empty($field->getValue())){
            $this->fields->add($field);
        }

        return $this;
    }

    /**
     * Обработка типа регистрации:
     * @return string
     */
    protected function getRegistrationChannel()
    {
        $registerType = self::SITE_REGISTRATION_CHANNEL;

        try{
            $sessionData = $this->session->get('regData');

            if (isset($sessionData['registerType'])) {
                $registerType = $sessionData['registerType'];
            }
        }catch (\RuntimeException $e){

        }

        return $registerType;
    }

    /**
     * @param null $type
     * @return null|string
     */
    protected function getSocialIdFromSession($type = null)
    {
        try{
            $sessionData = $this->session->get('regData');

            if (isset($sessionData['socialType']) && isset($sessionData['socialId']) && $type) {
                if ($sessionData['socialType'] == $type) {
                    return sprintf('%s %d', $sessionData['socialType'], $sessionData['socialId']);
                }
            }
        }catch (\RuntimeException $e){

        }

        return null;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return mixed
     */
    abstract protected function initFieldsFromRequest(Request $request);

    /**
     * @return mixed
     */
    abstract protected function initAddedFields();
}