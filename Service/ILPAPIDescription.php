<?php

namespace ADW\IlpCrmBundle\Service;

use ADW\IlpCrmBundle\Model\Request\ILPRequestBody;
use GuzzleHttp\Exception\ServerException;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\Serializer;
use Monolog\Logger;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use ADW\RestClientBundle\Client\ClientDescriptionInterface;
use ADW\RestClientBundle\Event\ExceptionEvent;
use ADW\RestClientBundle\Event\RequestEvent;
use ADW\RestClientBundle\Event\ResponseEvent;

/**
 * Class ILPAPIDescription
 * @package ADW\IlpCrmBundle
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ILPAPIDescription implements ClientDescriptionInterface
{
    /**
     * @var string
     */
    protected $host;

    /**
     * @var string
     */
    protected $key;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var string
     */
    protected $schema = 'https';

    /**
     * @var Serializer
     */
    protected $serializer;

    private $ilp_user;
    private $ilp_pass;
    private $ilp_secret_code;

    /**
     * @var TokenStorage
     */
    private $token_storage;

    /**
     * @param $host
     * @param Serializer $serializer
     * @param $user
     * @param $password
     * @param $secretCode
     * @param Logger $logger
     * @param TokenStorage $token_storage
     */
    public function __construct($host, Serializer $serializer, $user, $password, $secretCode, Logger $logger, TokenStorage $token_storage)
    {
        $this->host = $host;
        $this->serializer = $serializer;
        $this->ilp_user = $user;
        $this->ilp_pass = $password;
        $this->ilp_secret_code = $secretCode;
        $this->logger = $logger;
        $this->token_storage = $token_storage;
    }


    /**
     * @return string
     */
    public function getHost()
    {
        return $this->host;
    }

    /**
     * @return string
     */
    public function getSchema()
    {
        return $this->schema;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            [
                'event' => RequestEvent::NAME,
                'listener' => [$this, 'setHeaders'],
            ],
            [
                'event' => ExceptionEvent::NAME,
                'listener' => function (ExceptionEvent $event) {
                    $exception = $event->getException();

                    if ($exception instanceof ServerException) {
                        throw new \Exception($exception->getMessage(), $exception->getCode());
                    }

                    throw $exception;
                },
            ],
            [
                'event' => ResponseEvent::NAME,
                'listener' => [$this, 'responseStatusChecker']
            ]
        ];
    }

    /**
     * @param RequestEvent $event
     */
    public function setHeaders(RequestEvent $event)
    {
        $request = $event->getRequest();

        $auth = "Basic " . base64_encode(sprintf('%s:%s', $this->ilp_user, $this->ilp_pass));
        $request = $request->withHeader('Authorization', $auth);

        $event->setRequest($request);
    }

    /**
     * @return \Closure
     */
    public function getSerializer()
    {
        return function (ILPRequestBody $data, $format) {
            $data->setSecretcode($this->ilp_secret_code);

            $attrs = $this->token_storage->getToken() ? $this->token_storage->getToken()->getAttributes() : [];
            if (
                $this->token_storage->getToken() &&
                $this->token_storage->getToken()->isAuthenticated() && isset($attrs['crm_token'])

            ) {
                $token = $this->token_storage->getToken()->getAttribute('crm_token');
                $params = $data->getParams();
                $params['token'] = $token;

                $data->setParams($params);
            }
            return $this->serializer->serialize($data, $format);
        };
    }

    /**
     * @return \Closure
     */
    public function getDeserializer()
    {
        return function ($description, $data, $format, $model, array $context)
        {
            $jmsContext = DeserializationContext::create();
//            var_dump($data);
//            die();

            if (array_key_exists('groups', $context)) {
                $jmsContext->setGroups($context['groups']);
            }

            foreach ($context as $name => $value) {
                $jmsContext->setAttribute($name, $value);
            }

            $jmsContext->setAttribute('_context', clone $jmsContext);
            
            return $this->serializer->deserialize($data, $model, $format, $jmsContext);
        };
    }


    /**
     * @param ResponseEvent $event
     * @throws \Exception
     */
    public function responseStatusChecker(ResponseEvent $event)
    {
        $responseBody = (string)$event->getResponse()->getBody();

        $encoder = new XmlEncoder();
        $xmlBody = $encoder->decode($responseBody, 'xml');

        if (isset($xmlBody['error']) && $xmlBody['error'] == 2) {
            throw new \Exception($xmlBody['result']);
        }
    }

}