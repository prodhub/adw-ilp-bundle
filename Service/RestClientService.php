<?php

namespace ADW\IlpCrmBundle\Service;

use ADW\RestClientBundle\Client\Client;

/**
 * Class RestClientService
 * @package ADW\IlpCrmBundle\Service
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class RestClientService extends Client
{
    //
}