<?php

namespace ADW\IlpCrmBundle;

use ADW\IlpCrmBundle\DependencyInjection\Compiler\OverrideServiceCompilerPass;
use ADW\IlpCrmBundle\DependencyInjection\Security\Factory\IlpFactory;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Bundle\SecurityBundle\DependencyInjection\SecurityExtension;


/**
 * Class IlpCrmBundle
 * @package ADW\IlpCrmBundle
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class ADWIlpCrmBundle extends Bundle
{
    protected $container;

    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container)
    {
        parent::build($container);

        /** @var $extension SecurityExtension */
        $extension = $container->getExtension('security');
        $extension->addSecurityListenerFactory(new IlpFactory());
        $container->addCompilerPass(new OverrideServiceCompilerPass());
    }
}
