<?php

namespace ADW\IlpCrmBundle\Controller;

use ADW\IlpCrmBundle\Model\APIResponseModel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ADW\IlpCrmBundle\ParamsDefinition;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;


/**
 * Class CrmAuthController
 * @package ADW\IlpCrmBundle\Controller
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class CrmAuthController extends Controller
{

    /**
     * Авторизация происходит в IlpAuthenticationListener
     *
     * @Route("/auth/login", name="ilp_crm.user.login")
     * @ApiDoc(
     *  description="Авторизация",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="email"},
     *      {"name"="password", "dataType"="password", "required"=true, "description"="pass"}
     *  },
     *  section="ADW. ILP. Auth&Reg",
     * )
     */
    public function loginAction()
    {
        $response = new APIResponseModel();

        if ($this->get("security.authorization_checker")->isGranted('ROLE_USER')) {
            $response->setStatus(true);
        }else{
            $response->setErrorMessage(ParamsDefinition::AUTH_ERROR_MESSAGE);
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }

    /**
     * @Route("/auth/logout", name="ilp_crm.user.logout")
     */
    public function logoutAction()
    {
        return $this->redirectToRoute('homepage');
    }
}

