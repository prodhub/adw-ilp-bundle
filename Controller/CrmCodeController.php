<?php

namespace ADW\IlpCrmBundle\Controller;

use ADW\IlpCrmBundle\Model\APIResponseModel;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionGenerateActivationCodeRequest;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionGenerateActivationCodeResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use ADW\IlpCrmBundle\Method\Code\GenerateActivationMethodDescription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class CrmCodeController.php
 * @package ADW\IlpCrmBundle\Controller
 */
class CrmCodeController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/code/generate", name="ilp_crm.code.generate")
     * @ApiDoc(
     *  description="Первичная генерация кодов активации контактов  участника. ",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="Контакт участника"}
     *  },
     *  section="ADW. ILP. Codes section",
     * )
     * @Method({"GET", "POST"})
     */
    public function generateActivationAction(Request $request)
    {
        $response = new APIResponseModel();

        $requestDescription = new DescriptionGenerateActivationCodeRequest($request);


        if(!$requestDescription->isSuccessStatus()){
            $response->setBadFields($requestDescription->getErrors());
        }else{
            $ilp = $this->container->get('adw_ilp.service');
            try{
                $responseObj = $ilp->request(new GenerateActivationMethodDescription($requestDescription->getFieldsToArray()));
            }catch (\Exception $e){
                $response->setErrorMessage("Bab response. Status ".$e->getCode());
                return $response->getResponseByJson();
            }

            /** @var DescriptionGenerateActivationCodeResponse $responseDescription */
            $responseDescription = new DescriptionGenerateActivationCodeResponse($responseObj);

            if($responseDescription->getStatus() === true){
                $response->setStatus(true);
            }else{
                $response->setErrorMessage($responseDescription->getError())
                    ->setLimitations($responseDescription->getLimitations())
                    ->setBadFields($responseDescription->getBadFields())
                    ->setWarningFields($responseDescription->getWarningFields());
            }
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }

}

