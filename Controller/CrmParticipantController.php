<?php

namespace ADW\IlpCrmBundle\Controller;

use ADW\IlpCrmBundle\Method\Customer\CheckLoginUniquenessMethodDescription;
use ADW\IlpCrmBundle\Method\Customer\DropParticipantPasswordMethodDescription;
use ADW\IlpCrmBundle\Method\Customer\RequestPasswordRecoveryMethodDescription;
use ADW\IlpCrmBundle\Method\Customer\UpdateParticipantMethodDescription;
use ADW\IlpCrmBundle\Model\APIResponseModel;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionCheckLoginRequest;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionDropParticipantPasswordRequest;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionPasswordRecoveryRequest;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionUpdateParticipantRequest;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionCheckLoginResponse;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionDropParticipantPasswordResponse;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionPasswordRecoveryResponse;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionUpdateParticipantResponse;
use ADW\IlpCrmBundle\Security\Authentication\Token\IlpAuthUserToken;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class CrmParticipantController
 * @package ADW\IlpCrmBundle\Controller
 */
class CrmParticipantController extends Controller
{

    /**
     * Обновление полей пользователя
     * @Route("/ilp/participant/update", name="ilp_crm.participant.update")
     * @ApiDoc(
     *  description="Обновление полей пользователя",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="email"},
     *      {"name"="firstname", "dataType"="string", "required"=true, "description"="Имя"},
     *      {"name"="lastname", "dataType"="string", "required"=true, "description"="Фамилия"},
     *      {"name"="ismale", "dataType"="string", "required"=true, "description"="пол"},
     *      {"name"="city", "dataType"="string", "required"=true, "description"="Город"}
     *  },
     *  section="ADW. ILP. Participant",
     * )
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     */
    public function updateParticipantAction(Request $request)
    {
        $response = new APIResponseModel();

        if ($this->get("security.authorization_checker")->isGranted('ROLE_USER')) {

            $requestDescription = new DescriptionUpdateParticipantRequest($request);

            /** @var IlpAuthUserToken $tokenObj */
            $tokenObj = $this->get('security.token_storage')->getToken();
            $requestDescription->setToken($tokenObj);

            if(!$requestDescription->isSuccessStatus()){
                $response->setBadFields($requestDescription->getErrors());
            }else{
                /**
                 * Если прошли проверки, Отправляем запрос:
                 */
                $ilp = $this->container->get('adw_ilp.service');

                try{
                    $updParticipantObj = $ilp->request(new UpdateParticipantMethodDescription($requestDescription->getFieldsToArray()));
                }catch (\Exception $e){
                    $response->setErrorMessage("Bab response. Status ".$e->getCode());
                    return $response->getResponseByJson();
                }

                /** @var DescriptionUpdateParticipantResponse() $responseDescription */
                $responseDescription = new DescriptionUpdateParticipantResponse($updParticipantObj);

                if($responseDescription->getStatus() === true){

                    //Обновим данные пользователя
                    $participant = $this->get('adw_ilp.user_provider')->loadUserByToken($tokenObj->getAttribute('crm_token'));
                    $tokenObj->setUser($participant);

                    $response->setStatus(true);
                }else{
                    $response->setErrorMessage($responseDescription->getError())
                        ->setBadFields($responseDescription->getBadFields())
                        ->setLimitations($responseDescription->getLimitations())
                        ->setWarningFields($responseDescription->getWarningFields());
                }
            }


        }else{
            $response->setErrorMessage('You don\'t login');
        }


        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();

    }

    /**
     * Проверка логина на уникальность
     * @Route("/ilp/participant/check-login-uniqueness", name="ilp_crm.participant.check-login-uniqueness")
     * @ApiDoc(
     *  description="Проверка логина на уникальность",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="Проверяемы логин"}
     *  },
     *  section="ADW. ILP. Participant",
     * )
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     */
    public function checkLoginUniquenessAction(Request $request)
    {
        $response = new APIResponseModel();

        $requestDescription = new DescriptionCheckLoginRequest($request);


        if(!$requestDescription->isSuccessStatus()){
            $response->setBadFields($requestDescription->getErrors());
        }else{
            /**
             * Если прошли проверки, Отправляем запрос:
             */
            $ilp = $this->container->get('adw_ilp.service');

            try{
                $checkLoginObj = $ilp->request(new CheckLoginUniquenessMethodDescription($requestDescription->getFieldsToArray()));
            }catch (\Exception $e){
                $response->setErrorMessage("Bab response. Status ".$e->getCode());
                return $response->getResponseByJson();
            }

            /** @var DescriptionUpdateParticipantResponse() $responseDescription */
            $responseDescription = new DescriptionCheckLoginResponse($checkLoginObj);

            if($responseDescription->getStatus() === true){
                $response->setStatus(true);
            }else{
                $response->setErrorMessage($responseDescription->getError())
                    ->setLimitations($responseDescription->getLimitations());
            }
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }

    /**
     * СБРОС И ГЕНЕРАЦИЯ НОВОГО ПАРОЛЯ УЧАСТНИКА
     * @Route("/ilp/participant/drop-participant-password", name="ilp_crm.participant.drop-participant-password")
     * @ApiDoc(
     *  description="Cброс и генерация нового пароля участника",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="Логин участника"},
     *      {"name"="forced_password", "dataType"="string", "required"=true, "description"="Новый пароль"},
     *      {"name"="is_secure", "dataType"="string", "required"=false, "description"="Передаются ли в запросе  шифрованные данные? (Y/N)"}
     *  },
     *  section="ADW. ILP. Participant",
     * )
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     */
    public function dropParticipantPasswordAction(Request $request)
    {
        $response = new APIResponseModel();

        $requestDescription = new DescriptionDropParticipantPasswordRequest($request);


        if(!$requestDescription->isSuccessStatus()){
            $response->setBadFields($requestDescription->getErrors());
        }else{
            /**
             * Если прошли проверки, Отправляем запрос:
             */
            $ilp = $this->container->get('adw_ilp.service');

            try{
                $dropPasswordObj = $ilp->request(new DropParticipantPasswordMethodDescription($requestDescription->getFieldsToArray()));
            }catch (\Exception $e){
                $response->setErrorMessage("Bab response. Status ".$e->getCode());
                return $response->getResponseByJson();
            }

            /** @var DescriptionUpdateParticipantResponse() $responseDescription */
            $responseDescription = new DescriptionDropParticipantPasswordResponse($dropPasswordObj);

            if($responseDescription->getStatus() === true){
                $response->setStatus(true);
            }else{
                $response->setErrorMessage($responseDescription->getError())
                    ->setLimitations($responseDescription->getLimitations());
            }
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }


    /**
     * Запрос на сброс пароля участника и установку нового  пароля
     * @Route("/ilp/participant/recovery-password", name="ilp_crm.participant.recovery-password")
     * @ApiDoc(
     *  description="Запрос на сброс пароля участника и установку нового  пароля.",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="login", "dataType"="string", "required"=true, "description"="Логин участника"}
     *  },
     *  section="ADW. ILP. Participant",
     * )
     * @param Request $request
     * @return JsonResponse
     * @Method("POST")
     */
    public function requestPasswordRecoveryAction(Request $request)
    {
        $response = new APIResponseModel();

        $requestDescription = new DescriptionPasswordRecoveryRequest($request);


        if(!$requestDescription->isSuccessStatus()){
            $response->setBadFields($requestDescription->getErrors());
        }else{
            /**
             * Если прошли проверки, Отправляем запрос:
             */
            $ilp = $this->container->get('adw_ilp.service');

            try{
                $dropPasswordObj = $ilp->request(new RequestPasswordRecoveryMethodDescription($requestDescription->getFieldsToArray()));
            }catch (\Exception $e){
                $response->setErrorMessage("Bab response. Status ".$e->getCode());
                return $response->getResponseByJson();
            }

            /** @var DescriptionUpdateParticipantResponse() $responseDescription */
            $responseDescription = new DescriptionPasswordRecoveryResponse($dropPasswordObj);

            if($responseDescription->getStatus() === true){
                $response->setStatus(true);
            }else{
                $response->setErrorMessage($responseDescription->getError())
                    ->setLimitations($responseDescription->getLimitations());
            }
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }

}

