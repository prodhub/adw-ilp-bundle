<?php

namespace ADW\IlpCrmBundle\Controller;

use ADW\IlpCrmBundle\Model\APIResponseModel;
use ADW\IlpCrmBundle\Model\DescriptionRequest\DescriptionRegistrationRequest;
use ADW\IlpCrmBundle\Model\DescriptionResponse\DescriptionRegistrationResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use ADW\IlpCrmBundle\Method\Customer\RegisterParticipantMethodDescription;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


/**
 * Class CrmRegisterController.php
 * @package ADW\IlpCrmBundle\Controller
 * @author Roman Skvortsov <roman.skvortsov@isobar.ru>
 */
class CrmRegisterController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/auth/register", name="ilp_crm.user.register")
     * @ApiDoc(
     *  description="Регистрация",
     *  https=true,
     *  headers={
     *     {
     *        "name"="X-Requested-With",
     *        "description"="X-Requested-With",
     *        "default"="XMLHttpRequest"
     *     }
     *   },
     *  parameters={
     *      {"name"="email", "dataType"="string", "required"=true, "description"="email"},
     *      {"name"="password", "dataType"="password", "required"=true, "description"="password"},
     *      {"name"="firstname", "dataType"="string", "required"=true, "description"="Имя"},
     *      {"name"="lastname", "dataType"="string", "required"=true, "description"="Фамилия"},
     *      {"name"="birthdate", "dataType"="string", "required"=true, "description"="Дата рождения"},
     *      {"name"="ismale", "dataType"="string", "required"=true, "description"="Пол"},
     *      {"name"="city", "dataType"="string", "required"=true, "description"="Город"},
     *      {"name"="isrulesagreed", "dataType"="string", "required"=true, "description"="Согласие с договором (Y/N)"},
     *      {"name"="ispdagreed", "dataType"="string", "required"=true, "description"="Согласие на обработку ПДн  (Y/N)"},
     *      {"name"="ismailingagreed", "dataType"="string", "required"=true, "description"="Согласие на рассылки (Y/N)"}
     *  },
     *  section="ADW. ILP. Auth&Reg",
     * )
     * @ Method({"GET", "POST"})
     */
    public function registrationAction(Request $request)
    {
        $response = new APIResponseModel();

        $requestDescription = new DescriptionRegistrationRequest($request);


        if(!$requestDescription->isSuccessStatus()){
            $response->setBadFields($requestDescription->getErrors());
        }else{
            /**
             * Если прошли проверки, Регистрируем участника:
             */
            $ilp = $this->container->get('adw_ilp.service');
            try{
                $registerObj = $ilp->request(new RegisterParticipantMethodDescription($requestDescription->getFieldsToArray()));
            }catch (\Exception $e){
                $response->setErrorMessage("Bab response. Status ".$e->getCode());
                return $response->getResponseByJson();
            }

            /** @var DescriptionRegistrationResponse $responseDescription */
            $responseDescription = new DescriptionRegistrationResponse($registerObj);

            if($responseDescription->getStatus() === true){
                $response->setStatus(true);
            }else{
                $response->setErrorMessage($responseDescription->getError())
                    ->setLimitations($responseDescription->getLimitations())
                    ->setBadFields($responseDescription->getBadFields())
                    ->setWarningFields($responseDescription->getWarningFields());
            }
        }

        /**
         * Возвращаем ответ:
         */
        return $response->getResponseByJson();
    }

}

